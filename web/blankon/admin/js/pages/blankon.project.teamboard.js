var BlankonProjectTeamBoard = function () {

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            BlankonProjectTeamBoard.handleTabs();
        },

        // =========================================================================
        // HANDLE TABS
        // =========================================================================
        handleTabs: function () {
            (function($, window, document, undefined) {
                'use strict';

                // init cubeportfolio
                $('#js-grid-lightbox-teamboard').cubeportfolio({
                    filters: '#js-filters-lightbox-teamboard2',
                    search: '#js-search-teamboard',

                    layoutMode: 'grid',
                    mediaQueries: [{
                        width: 1500,
                        cols: 5
                    }, {
                        width: 1100,
                        cols: 4
                    }, {
                        width: 800,
                        cols: 3
                    }, {
                        width: 480,
                        cols: 2
                    }, {
                        width: 320,
                        cols: 1
                    }],
                    defaultFilter: '*',
                    animationType: 'rotateSides',
                    gapHorizontal: 10,
                    gapVertical: 10,
                    gridAdjustment: 'responsive',
                    caption: 'zoom',
                    displayType: 'sequentially',
                    displayTypeSpeed: 100,

                    // lightbox
                    lightboxDelegate: '.cbp-lightbox',
                    lightboxGallery: true,
                    lightboxTitleSrc: 'data-title',
                    lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',


                });
            })(jQuery, window, document);
        }

    };

}();

// Call main app init
BlankonProjectTeamBoard.init();