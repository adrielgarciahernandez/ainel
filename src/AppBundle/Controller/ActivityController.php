<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Activity;
use AppBundle\Entity\ActivityComplexity;
use AppBundle\Entity\ActivityComplexityValue;
use AppBundle\Form\ActivityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Activity controller.
 *
 * @Route("/manager/activity")
 */
class ActivityController extends Controller
{
    /**
     * Lists all activity entities.
     *
     * @Route("/", name="activity_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $activitys = $em->getRepository(Activity::class)->findAll();

        $deleteForms = array();
        foreach ($activitys as $activity)
        {
            $deleteForms[$activity->getId()] = $this->createDeleteForm($activity)->createView();
        }

        return $this->render('activity/index.html.twig', array(
            'activitys' => $activitys,
            'deleteForms' => $deleteForms
        ));
    }

    /**
     * Creates a new activity entity.
     *
     * @Route("/new/", name="activity_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $activity = new Activity();
        $complexities = $em->getRepository(ActivityComplexity::class)->findAll();

        foreach ($complexities as $complexity)
        {
            $currentValue = new ActivityComplexityValue();
            $currentValue->setComplexity($complexity);
            $currentValue->setValue(0);
            $activity->addValue($currentValue);
        }

        $form = $this->createForm(ActivityType::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($activity);
            $em->flush();

            $this->addFlash('created-ok' , 'Actividad creado satisfactoriamente.');

            $submitButton = $request->get('submit-button');
            if ($submitButton == 'create')
            {
                return $this->redirectToRoute('activity_show', array('id' => $activity->getId()));
            }
            return $this->redirectToRoute('activity_new');
        }

        return $this->render('activity/new.html.twig', array(
            'activity' => $activity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a activity entity.
     *
     * @Route("/{id}/", name="activity_show")
     * @Method("GET")
     */
    public function showAction(Activity $activity)
    {
        $deleteForm = $this->createDeleteForm($activity);

        return $this->render('activity/show.html.twig', array(
            'activity' => $activity,
            'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing activity entity.
     *
     * @Route("/{id}/edit/", name="activity_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Activity $activity)
    {
        $em = $this->getDoctrine()->getManager();
        $complexities = $em->getRepository(ActivityComplexity::class)->findAll();

        foreach ($complexities as $complexity)
        {
            if(!$activity->hasValue($complexity))
            {
                $currentValue = new ActivityComplexityValue();
                $currentValue->setComplexity($complexity);
                $currentValue->setValue(0);
                $activity->addValue($currentValue);
            }
        }

        $editForm = $this->createForm(ActivityType::class, $activity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            $this->addFlash('edited-ok' , 'Actividad editado satisfactoriamente.');

            return $this->redirectToRoute('activity_edit', array('id' => $activity->getId()));
        }

        return $this->render('activity/edit.html.twig', array(
            'activity' => $activity,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a activity entity.
     *
     * @Route("/{id}/", name="activity_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Activity $activity)
    {
        $form = $this->createDeleteForm($activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($activity->getValues() as $value)
            {
                $em->remove($value);
            }

            $em->remove($activity);

            try
            {
                $em->flush();
                $this->addFlash('deleted-ok' , 'Actividad ' . $activity . ' eliminada satisfactoriamente.');
            }
            catch (\Exception $e)
            {
                $this->addFlash('deleted-error' , 'El Actividad ' . $activity . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
            }
        }

        return $this->redirectToRoute('activity_index');
    }

    /**
     * Creates a form to delete a activity entity.
     *
     * @param Activity $activity The activity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Activity $activity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('activity_delete', array('id' => $activity->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
