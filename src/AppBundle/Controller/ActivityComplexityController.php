<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ActivityComplexity;
use AppBundle\Form\ActivityComplexityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * ActivityComplexity controller.
 *
 * @Route("/manager/activityComplexity")
 */
class ActivityComplexityController extends Controller
{
    /**
     * Lists all activityComplexity entities.
     *
     * @Route("/", name="activityComplexity_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $activityComplexitys = $em->getRepository(ActivityComplexity::class)->findAll();

        $deleteForms = array();
        foreach ($activityComplexitys as $activityComplexity)
        {
            $deleteForms[$activityComplexity->getId()] = $this->createDeleteForm($activityComplexity)->createView();
        }

        return $this->render('activityComplexity/index.html.twig', array(
            'activityComplexitys' => $activityComplexitys,
            'deleteForms' => $deleteForms
        ));
    }

    /**
     * Creates a new activityComplexity entity.
     *
     * @Route("/new/", name="activityComplexity_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $activityComplexity = new ActivityComplexity();
        $form = $this->createForm(ActivityComplexityType::class, $activityComplexity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($activityComplexity);
            $em->flush();

            $this->addFlash('created-ok' , 'Tipo de Proyecto creado satisfactoriamente.');

            $submitButton = $request->get('submit-button');
            if ($submitButton == 'create')
            {
                return $this->redirectToRoute('activityComplexity_show', array('id' => $activityComplexity->getId()));
            }
            return $this->redirectToRoute('activityComplexity_new');
        }

        return $this->render('activityComplexity/new.html.twig', array(
            'activityComplexity' => $activityComplexity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a activityComplexity entity.
     *
     * @Route("/{id}/", name="activityComplexity_show")
     * @Method("GET")
     */
    public function showAction(ActivityComplexity $activityComplexity)
    {
        $deleteForm = $this->createDeleteForm($activityComplexity);

        return $this->render('activityComplexity/show.html.twig', array(
            'activityComplexity' => $activityComplexity,
            'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing activityComplexity entity.
     *
     * @Route("/{id}/edit/", name="activityComplexity_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ActivityComplexity $activityComplexity)
    {
        $editForm = $this->createForm(ActivityComplexityType::class, $activityComplexity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('edited-ok' , 'Tipo de Proyecto editado satisfactoriamente.');

            return $this->redirectToRoute('activityComplexity_edit', array('id' => $activityComplexity->getId()));
        }

        return $this->render('activityComplexity/edit.html.twig', array(
            'activityComplexity' => $activityComplexity,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a activityComplexity entity.
     *
     * @Route("/{id}/", name="activityComplexity_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ActivityComplexity $activityComplexity)
    {
        $form = $this->createDeleteForm($activityComplexity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($activityComplexity);

            try
            {
                $em->flush();
                $this->addFlash('deleted-ok' , 'Tipo de Proyecto ' . $activityComplexity . ' eliminada satisfactoriamente.');
            }
            catch (\Exception $e)
            {
                $this->addFlash('deleted-error' , 'El Tipo de Proyecto ' . $activityComplexity . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
            }
        }

        return $this->redirectToRoute('activityComplexity_index');
    }

    /**
     * Creates a form to delete a activityComplexity entity.
     *
     * @param ActivityComplexity $activityComplexity The activityComplexity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ActivityComplexity $activityComplexity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('activityComplexity_delete', array('id' => $activityComplexity->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
