<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ProjectKind;
use AppBundle\Form\ProjectKindType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * ProjectKind controller.
 *
 * @Route("/manager/projectKind")
 */
class ProjectKindController extends Controller
{
    /**
     * Lists all projectKind entities.
     *
     * @Route("/", name="projectKind_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $projectKinds = $em->getRepository(ProjectKind::class)->findAll();

        $deleteForms = array();
        foreach ($projectKinds as $projectKind)
        {
            $deleteForms[$projectKind->getId()] = $this->createDeleteForm($projectKind)->createView();
        }

        return $this->render('projectKind/index.html.twig', array(
            'projectKinds' => $projectKinds,
            'deleteForms' => $deleteForms
        ));
    }

    /**
     * Creates a new projectKind entity.
     *
     * @Route("/new/", name="projectKind_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $projectKind = new ProjectKind();
        $form = $this->createForm(ProjectKindType::class, $projectKind);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectKind);
            $em->flush();

            $this->addFlash('created-ok' , 'Tipo de Proyecto creado satisfactoriamente.');

            $submitButton = $request->get('submit-button');
            if ($submitButton == 'create')
            {
                return $this->redirectToRoute('projectKind_show', array('id' => $projectKind->getId()));
            }
            return $this->redirectToRoute('projectKind_new');
        }

        return $this->render('projectKind/new.html.twig', array(
            'projectKind' => $projectKind,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a projectKind entity.
     *
     * @Route("/{id}/", name="projectKind_show")
     * @Method("GET")
     */
    public function showAction(ProjectKind $projectKind)
    {
        $deleteForm = $this->createDeleteForm($projectKind);

        return $this->render('projectKind/show.html.twig', array(
            'projectKind' => $projectKind,
            'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing projectKind entity.
     *
     * @Route("/{id}/edit/", name="projectKind_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ProjectKind $projectKind)
    {
        $editForm = $this->createForm(ProjectKindType::class, $projectKind);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('edited-ok' , 'Tipo de Proyecto editado satisfactoriamente.');

            return $this->redirectToRoute('projectKind_edit', array('id' => $projectKind->getId()));
        }

        return $this->render('projectKind/edit.html.twig', array(
            'projectKind' => $projectKind,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a projectKind entity.
     *
     * @Route("/{id}/", name="projectKind_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ProjectKind $projectKind)
    {
        $form = $this->createDeleteForm($projectKind);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($projectKind);

            try
            {
                $em->flush();
                $this->addFlash('deleted-ok' , 'Tipo de Proyecto ' . $projectKind . ' eliminada satisfactoriamente.');
            }
            catch (\Exception $e)
            {
                $this->addFlash('deleted-error' , 'El Tipo de Proyecto ' . $projectKind . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
            }
        }

        return $this->redirectToRoute('projectKind_index');
    }

    /**
     * Creates a form to delete a projectKind entity.
     *
     * @param ProjectKind $projectKind The projectKind entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProjectKind $projectKind)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('projectKind_delete', array('id' => $projectKind->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
