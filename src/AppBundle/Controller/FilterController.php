<?php

namespace AppBundle\Controller;

use AppBundle\Entity\FilterProject;
use AppBundle\Entity\PaymentFilter;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectLog;
use AppBundle\Entity\ProjectStage;
use AppBundle\Entity\ProjectStageActivity;
use AppBundle\Entity\Stage;
use AppBundle\Entity\StatusEnum;
use AppBundle\Form\FilterProjectType;
use AppBundle\Form\NextStageType;
use AppBundle\Form\PaymentFilterType;
use AppBundle\Form\ProjectEditType;
use AppBundle\Form\ProjectLogType;
use AppBundle\Form\ProjectStageActivityType;
use AppBundle\Form\ProjectType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Filter controller.
 *
 * @Route("/manager/filter")
 */
class FilterController extends Controller
{
    /**
     * Creates a new project entity.
     *
     * @Route("/index/", name="filter_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $filter = new FilterProject();
        $form = $this->createForm(FilterProjectType::class, $filter);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        
        $projects = [];
        $filterButton = 'clean';

        if ($form->isSubmitted()) {
            $filterButton = $request->get('submit-button');
            if ($filterButton == 'filter')
            {
                $projects = $em->getRepository(FilterProject::class)->filter($filter);
            }
            else
            {
                $filter = new FilterProject();
                $form = $this->createForm(FilterProjectType::class, $filter);
            }
        }

        $tableHeader = ['OT',
            'Número',
            'Fecha de Solicitud',
            'Fecha de Entrega',
            'Estado',
            'Proyectista',
            'Tipos',
            'Dirección',
            'Comuna',
            'Clase',
            'Área',
            'Plazo',
            'Ejecutivo',
            'Especialista'];

        return $this->render('filter/index.html.twig', array(
            'filter' => $filter,
            'form' => $form->createView(),
            'projects' => $projects,
            'tableHeader' => $tableHeader,
            'filterButton' => $filterButton
        ));
    }

    /**
     * Creates a new project entity.
     *
     * @Route("/payment/", name="filter_payment")
     * @Method({"GET", "POST"})
     */
    public function paymentAction(Request $request)
    {
        $filter = new PaymentFilter();
        $form = $this->createForm(PaymentFilterType::class, $filter);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $projectsQuoted = $em->getRepository(Project::class)->findBy(['status'=>StatusEnum::STATUS_QUOTED]);

        return $this->render('filter/payment.html.twig', array(
            'filter' => $filter,
            'form' => $form->createView(),
            'projectsQuoted' => $projectsQuoted,
        ));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/payment/quote/excel/{_filename}.{_format}", name="filter_payment_excel", requirements={"_format"="csv|xls|xlsx"})
     * @Method({"GET", "POST"})
     */
    public function quoteExcelAction(Request $request, $_format)
    {
        //var_dump('llegue');die();
        $filter = new PaymentFilter();
        $form = $this->createForm(PaymentFilterType::class, $filter);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $projects = [];
        $projectsTotals = [];
        $projectsTotal = [];
        $projectsWorkers = [];

        if ($form->isSubmitted()) {
            $filterButton = $request->get('submit-button');

            if($filterButton == 'filter-activities' || $filterButton == 'all-activities')
            {
                if ($filterButton == 'filter-activities')
                {
                    $projects = $em->getRepository(PaymentFilter::class)->filter($filter);
                }
                else
                {
                    $projects = $em->getRepository(Project::class)->findBy(['status'=>StatusEnum::STATUS_QUOTED]);
                }

                foreach ($projects as $project)
                {
                    $totals = ['total'=>0, 'totalFactor'=>0, 'totalBonus'=>0, 'totalTerm'=>0, 'totalPenalty'=>0, 'totalFinal'=>0 ];

                    foreach ($project->getPaymentStatus()->getActivities() as $activity)
                    {
                        $totals['total'] += $activity->getPrice() * $activity->getCount();
                    }

                    $totals['totalFactor'] = $totals['total'] * $project->getPaymentStatus()->getFactor();
                    $totals['totalBonus'] = $totals['total'] * $project->getPaymentStatus()->getBonus();
                    $totals['totalPenalty'] = $totals['total'] * $project->getPaymentStatus()->getPenalty();
                    if($project->getStatus() == StatusEnum::STATUS_PAID)
                    {
                        $totals['totalTerm'] = $totals['total'] * $project->getPaymentStatus()->getTermFactor();
                    }
                    else
                    {
                        $totals['totalTerm'] = $totals['total'] * $project->getTerm()->getIncrease();
                    }

                    $totals['totalFinal'] = $totals['total'] + $totals['totalFactor'] + $totals['totalBonus'] + $totals['totalTerm'] - $totals['totalPenalty'];

                    $projectsTotals[$project->getId()] = $totals;
                }

                return $this->render('filter/quote.'.$_format.'.twig', array(
                    'projects' => $projects,
                    'projectsTotals' => $projectsTotals
                ));
            }
            else
            {
                if ($filterButton == 'filter-worker')
                {
                    $projects = $em->getRepository(PaymentFilter::class)->filter($filter);
                }
                else
                {
                    $projects = $em->getRepository(Project::class)->findBy(['status'=>StatusEnum::STATUS_QUOTED]);
                }

                $total = 0;

                $workers = [];

                foreach ($projects as $project)
                {
                    foreach ($project->getPaymentStatus()->getActivities() as $activity)
                    {
                        $total += $activity->getPrice() * $activity->getCount();

                        $workerId = $activity->getActivity()->getExecutor()->getId();
                        if(!isset($workers[$workerId]))
                        {
                            $workers[$workerId] = ['worker'=>$activity->getActivity()->getExecutor(),
                                'activities'=>[],
                                'total'=>0];
                        }

                        $workers[$workerId]['activities'][] = $activity;
                        $workers[$workerId]['total'] += $activity->getPrice() * $activity->getCount();
                    }
                }

                return $this->render('filter/quote-worker.'.$_format.'.twig', array(
                    'workers' => $workers,
                    'total' => $total
                ));
            }
        }
    }
}
