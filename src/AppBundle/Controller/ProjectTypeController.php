<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Activity;
use AppBundle\Entity\ProjectType;
use AppBundle\Entity\ProjectTypeStage;
use AppBundle\Entity\Stage;
use AppBundle\Form\ProjectTypeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * ProjectType controller.
 *
 * @Route("/manager/projectType")
 */
class ProjectTypeController extends Controller
{
    /**
     * Lists all projectType entities.
     *
     * @Route("/", name="projectType_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $projectTypes = $em->getRepository(ProjectType::class)->findAll();

        $deleteForms = array();
        foreach ($projectTypes as $projectType)
        {
            $deleteForms[$projectType->getId()] = $this->createDeleteForm($projectType)->createView();
        }

        return $this->render('projectType/index.html.twig', array(
            'projectTypes' => $projectTypes,
            'deleteForms' => $deleteForms
        ));
    }

    /**
     * Creates a new projectType entity.
     *
     * @Route("/new/", name="projectType_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $projectType = new ProjectType();
        $form = $this->createForm(ProjectTypeType::class, $projectType);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted()) {

            $stages = $request->get('stages');
            $activities = $request->get('activities');

            if($stages != null)
            {
                $stagesIds = array_keys($stages);
                for ($i = 0; $i < count($stagesIds); $i++)
                {
                    $projectTypeStage = new ProjectTypeStage();
                    $projectTypeStage->setStageOrder($i);
                    $projectTypeStage->setStage($em->getRepository(Stage::class)->find($stagesIds[$i]));

                    if($activities != null && isset($activities[$stagesIds[$i]]))
                    {
                        $stageActivities = $activities[$stagesIds[$i]];
                        foreach ($stageActivities as $activity)
                        {
                            $projectTypeStage->addActivity($em->getRepository(Activity::class)->find($activity));
                        }
                    }

                    $projectType->addStage($projectTypeStage);
                }
            }

            if ($form->isValid())
            {
                $em->persist($projectType);
                $em->flush();

                $this->addFlash('created-ok' , 'Tipo de Proyecto creado satisfactoriamente.');

                $submitButton = $request->get('submit-button');
                if ($submitButton == 'create')
                {
                    return $this->redirectToRoute('projectType_show', array('id' => $projectType->getId()));
                }
                return $this->redirectToRoute('projectType_new');
            }
        }


        $allStages = $em->getRepository(Stage::class)->findAll();
        $stages = [];
        foreach ($allStages as $s)
        {
            if(!$projectType->hasStage($s))
            {
                $stages[] = $s;
            }
        }

        $activities = $em->getRepository(Activity::class)->findAll();

        return $this->render('projectType/new.html.twig', array(
            'projectType' => $projectType,
            'stages' => $stages,
            'activities' => $activities,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a projectType entity.
     *
     * @Route("/{id}/", name="projectType_show")
     * @Method("GET")
     */
    public function showAction(ProjectType $projectType)
    {
        $deleteForm = $this->createDeleteForm($projectType);

        return $this->render('projectType/show.html.twig', array(
            'projectType' => $projectType,
            'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a projectType entity.
     *
     * @Route("/methodology/stages/", name="projectType_stages")
     * @Method("POST")
     */
    public function stagesAction(Request $request)
    {
        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $stages = [];
        $template = 'stages';

        if($id > 0)
        {
            $projectType = $em->getRepository(ProjectType::class)->find($id);
            $template .= 'Methodology';

            foreach ($projectType->getStages() as $stage)
            {
                $stages[] = $stage->getStage();
            }
        }
        else
        {
            $stages = $em->getRepository(Stage::class)->findAll();
            $template .= 'All';
        }


        $html = $this->renderView('projectType/'.$template.'.html.twig', array(
            'stages' => $stages,
        ));

        $result = ['status'=>'succes', 'html'=>$html];

        return new JsonResponse($result);
    }

    /**
     * Displays a form to edit an existing projectType entity.
     *
     * @Route("/{id}/edit/", name="projectType_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ProjectType $projectType)
    {
        $editForm = $this->createForm(ProjectTypeType::class, $projectType);
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($editForm->isSubmitted()) {

            $stages = $request->get('stages');
            $activities = $request->get('activities');

            foreach ($projectType->getStages() as $projectStage)
            {
                $projectStage->getActivities()->clear();

                $projectType->removeStage($projectStage);
                $em->remove($projectStage);
            }

            if($stages != null)
            {
                $stagesIds = array_keys($stages);
                for ($i = 0; $i < count($stagesIds); $i++)
                {
                    $projectTypeStage = new ProjectTypeStage();
                    $projectTypeStage->setStageOrder($i);
                    $projectTypeStage->setStage($em->getRepository(Stage::class)->find($stagesIds[$i]));

                    if($activities != null && isset($activities[$stagesIds[$i]]))
                    {
                        $stageActivities = $activities[$stagesIds[$i]];
                        foreach ($stageActivities as $activity)
                        {
                            $projectTypeStage->addActivity($em->getRepository(Activity::class)->find($activity));
                        }
                    }

                    $projectType->addStage($projectTypeStage);
                }
            }

            if ($editForm->isValid())
            {
                $em->persist($projectType);
                $em->flush();

                $this->addFlash('edited-ok' , 'Tipo de Proyecto editado satisfactoriamente.');

                return $this->redirectToRoute('projectType_edit', array('id' => $projectType->getId()));
            }
        }

        $allStages = $em->getRepository(Stage::class)->findAll();
        $stages = [];
        foreach ($allStages as $s)
        {
            if(!$projectType->hasStage($s))
            {
                $stages[] = $s;
            }
        }

        $activities = $em->getRepository(Activity::class)->findAll();

        return $this->render('projectType/edit.html.twig', array(
            'projectType' => $projectType,
            'stages' => $stages,
            'activities' => $activities,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a projectType entity.
     *
     * @Route("/{id}/", name="projectType_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ProjectType $projectType)
    {
        $form = $this->createDeleteForm($projectType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            foreach ($projectType->getStages() as $projectStage)
            {
                $projectStage->getActivities()->clear();

                $projectType->removeStage($projectStage);
                $em->remove($projectStage);
            }

            $em->remove($projectType);

            try
            {
                $em->flush();
                $this->addFlash('deleted-ok' , 'Tipo de Proyecto ' . $projectType . ' eliminada satisfactoriamente.');
            }
            catch (\Exception $e)
            {
                $this->addFlash('deleted-error' , 'El Tipo de Proyecto ' . $projectType . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
            }
        }

        return $this->redirectToRoute('projectType_index');
    }

    /**
     * Creates a form to delete a projectType entity.
     *
     * @param ProjectType $projectType The projectType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProjectType $projectType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('projectType_delete', array('id' => $projectType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
