<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Project;
use AppBundle\Entity\StatusEnum;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/app/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        $projects = [StatusEnum::STATUS_SAVED => 0,
            StatusEnum::STATUS_ACTIVATED => 0,
            StatusEnum::STATUS_REVIEWED => 0,
            StatusEnum::STATUS_APPROVED => 0,
            StatusEnum::STATUS_QUOTED => 0,
            StatusEnum::STATUS_PAID => 0,
            StatusEnum::STATUS_FROZEN => 0,
            StatusEnum::STATUS_CANCELED => 0,
                    ];

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository(Project::class);

        foreach (array_keys($projects) as $status)
        {
            $count = count($repo->findBy(['status'=>$status]));
            $projects[$status] = $count;
        }

        $user = $this->getUser();
        $userProjects = $em->getRepository(Project::class)->findBy(['designer'=>$user->getId(), 'status'=>StatusEnum::STATUS_ACTIVATED]);
        $userProjectTimeStatus = ['in-time'=>0, 'closed'=>0, 'to-close'=>0];
        foreach ($userProjects as $project)
        {
            $userProjectTimeStatus[$project->getTimeStatus()]++;
        }

        return $this->render('default/index.html.twig', ['projects'=>$projects, 'userProjectTimeStatus'=>$userProjectTimeStatus]);
    }
}
