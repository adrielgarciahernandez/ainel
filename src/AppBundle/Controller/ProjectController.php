<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ActivityComplexity;
use AppBundle\Entity\PaymentStatus;
use AppBundle\Entity\PaymentStatusActivity;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectLog;
use AppBundle\Entity\ProjectStage;
use AppBundle\Entity\ProjectStageActivity;
use AppBundle\Entity\Stage;
use AppBundle\Entity\StatusEnum;
use AppBundle\Form\NextStageType;
use AppBundle\Form\PaymentStatusType;
use AppBundle\Form\ProjectEditType;
use AppBundle\Form\ProjectLogType;
use AppBundle\Form\ProjectStageActivityType;
use AppBundle\Form\ProjectType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Project controller.
 *
 * @Route("/project")
 */
class ProjectController extends Controller
{
    /**
     * Lists all project entities.
     *
     * @Route("/manager/index/{status}/", name="project_index")
     * @Method("GET")
     */
    public function indexAction($status)
    {
        $em = $this->getDoctrine()->getManager();

        $projects = $em->getRepository(Project::class)->findBy(['status'=>$status]);

        $deleteForms = array();
        foreach ($projects as $project)
        {
            $deleteForms[$project->getId()] = $this->createDeleteForm($project)->createView();
        }

        return $this->render('project/index.html.twig', array(
            'projects' => $projects,
            'deleteForms' => $deleteForms,
            'status' => $status
        ));
    }

    /**
     * Lists all project entities.
     *
     * @Route("/director/index/designer/my_projects", name="project_index_designer")
     * @Method("GET")
     */
    public function indexDesignerAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $projects = $em->getRepository(Project::class)->findBy(['designer'=>$user->getId(), 'status'=>StatusEnum::STATUS_ACTIVATED]);

        return $this->render('project/designer/index.html.twig', array(
            'projects' => $projects,
        ));
    }

    /**
     * Creates a new project entity.
     *
     * @Route("/manager/new/", name="project_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted()) {

            $stages = $request->get('stages');
            $stagesStart = $request->get('stages-start');
            $stagesEnd = $request->get('stages-end');
            $stagesChecked = $request->get('stages-checked');

            if($stages != null && $stagesChecked != null)
            {
                foreach ($stages as $stageId)
                {
                    if (isset($stagesChecked[$stageId]))
                    {
                        $currentStage = $em->getRepository(Stage::class)->find($stageId);
                        $projectStage = new ProjectStage();
                        $projectStage->setStage($currentStage);

                        if ($stagesStart[$stageId] != "")
                        {
                            $startDate = \DateTime::createFromFormat('d/m/Y', $stagesStart[$stageId]);
                            $projectStage->setProgramationStartDate($startDate);
                        }

                        if ($stagesEnd[$stageId] != "")
                        {
                            $endDate = \DateTime::createFromFormat('d/m/Y', $stagesEnd[$stageId]);
                            $projectStage->setProgramationEndDate($endDate);
                        }

                        $project->addStage($projectStage);
                    }
                }
            }

            if($form->isValid())
            {
                if($project->getModification() != null)
                {
                    $project->setName($project->getOt()->__toString() . $project->getOtNumber() . ' - ' . $project->getModification());
                }
                else
                {
                    $project->setName($project->getOt()->__toString() . $project->getOtNumber());
                }

                $project->setStatus(StatusEnum::STATUS_SAVED);

                $em->persist($project);
                $em->flush();

                $this->addFlash('created-ok' , 'Actividad creado satisfactoriamente.');

                $submitButton = $request->get('submit-button');
                if ($submitButton == 'create')
                {
                    return $this->redirectToRoute('project_show', array('id' => $project->getId()));
                }

                return $this->redirectToRoute('project_new');
            }
        }

        $allStages = $em->getRepository(Stage::class)->findAll();
        $stages = [];
        foreach ($allStages as $s)
        {
            if(!$project->hasStage($s))
            {
                $stages[] = $s;
            }
        }

        return $this->render('project/new.html.twig', array(
            'project' => $project,
            'form' => $form->createView(),
            'stages' => $stages
        ));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/common/{id}/", name="project_show")
     * @Method("GET")
     */
    public function showAction(Project $project)
    {
        if (!$this->getUser()->getCharge()->getIsProjectManager())
        {
            if ($this->getUser()->getId() != $project->getDesigner()->getId() || $project->getRealStatus() != StatusEnum::STATUS_ACTIVATED)
            {
                return $this->redirectToRoute('homepage');
            }
        }

        $deleteForm = $this->createDeleteForm($project);

        $logForms = ['activate' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
            'back' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
            'approve' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
            'pay' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
            'frozen' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
            'cancel' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView()
        ];

        return $this->render('project/show.html.twig', array(
            'project' => $project,
            'deleteForm' => $deleteForm->createView(),
            'logForms' => $logForms
        ));
    }

    /**
     * Displays a form to edit an existing project entity.
     *
     * @Route("/manager/{id}/edit/", name="project_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Project $project)
    {
        if($project->getRealStatus() == StatusEnum::STATUS_REVIEWED || $project->getRealStatus() == StatusEnum::STATUS_PAID)
        {
            return $this->redirectToRoute('project_show', array('id' => $project->getId()));
        }

        if($project->getRealStatus() == StatusEnum::STATUS_SAVED)
        {
            $editForm = $this->createForm(ProjectType::class, $project);
            $editView = 'project/edit-all.html.twig';
        }
        else if($project->getRealStatus() == StatusEnum::STATUS_ACTIVATED)
        {
            $editForm = $this->createForm(ProjectEditType::class, $project);
            $editView = 'project/edit-active.html.twig';
        }
        else
        {
            $editForm = $this->createForm(ProjectEditType::class, $project);
            $editView = 'project/edit.html.twig';
        }
        $editForm->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($editForm->isSubmitted()) {

            if($project->getRealStatus() == StatusEnum::STATUS_SAVED) {
                foreach ($project->getStages() as $projectStage) {
                    $project->removeStage($projectStage);
                    $em->remove($projectStage);
                }

                $stages = $request->get('stages');
                $stagesStart = $request->get('stages-start');
                $stagesEnd = $request->get('stages-end');
                $stagesChecked = $request->get('stages-checked');

                if($stages != null && $stagesChecked != null)
                {
                    foreach ($stages as $stageId)
                    {
                        if (isset($stagesChecked[$stageId]))
                        {
                            $currentStage = $em->getRepository(Stage::class)->find($stageId);
                            $projectStage = new ProjectStage();
                            $projectStage->setStage($currentStage);

                            if ($stagesStart[$stageId] != "")
                            {
                                $startDate = \DateTime::createFromFormat('d/m/Y', $stagesStart[$stageId]);
                                $projectStage->setProgramationStartDate($startDate);
                            }

                            if ($stagesEnd[$stageId] != "")
                            {
                                $endDate = \DateTime::createFromFormat('d/m/Y', $stagesEnd[$stageId]);
                                $projectStage->setProgramationEndDate($endDate);
                            }

                            $project->addStage($projectStage);
                        }
                    }
                }
            }

            if($project->getRealStatus() == StatusEnum::STATUS_ACTIVATED) {

                $stages = $request->get('stages');
                $stagesStart = $request->get('stages-start');
                $stagesEnd = $request->get('stages-end');

                if($stages != null)
                {
                    foreach ($stages as $stageId)
                    {
                        $projectStage = $project->getStageById($stageId);

                        if ($stagesStart[$stageId] != "")
                        {
                            $startDate = \DateTime::createFromFormat('d/m/Y', $stagesStart[$stageId]);
                            $projectStage->setProgramationStartDate($startDate);
                        }

                        if ($stagesEnd[$stageId] != "")
                        {
                            $endDate = \DateTime::createFromFormat('d/m/Y', $stagesEnd[$stageId]);
                            $projectStage->setProgramationEndDate($endDate);
                        }
                    }
                }
            }

            if ($editForm->isValid())
            {
                if($project->getModification() != null)
                {
                    $project->setName($project->getOt()->__toString() . $project->getOtNumber() . '-' . $project->getModification());
                }
                else
                {
                    $project->setName($project->getOt()->__toString() . $project->getOtNumber());
                }

                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('edited-ok' , 'Actividad editado satisfactoriamente.');

                return $this->redirectToRoute('project_edit', array('id' => $project->getId()));
            }
        }


        $allStages = $em->getRepository(Stage::class)->findAll();
        $stages = [];
        foreach ($allStages as $s)
        {
            if(!$project->hasStage($s))
            {
                $stages[] = $s;
            }
        }

        return $this->render($editView, array(
            'project' => $project,
            'form' => $editForm->createView(),
            'stages' => $stages
        ));
    }

    /**
     * Deletes a project entity.
     *
     * @Route("/manager/{id}/", name="project_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Project $project)
    {
        $form = $this->createDeleteForm($project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if($project->getRealStatus() == StatusEnum::STATUS_SAVED) {
                foreach ($project->getStages() as $projectStage) {
                    $project->removeStage($projectStage);
                    $em->remove($projectStage);
                }

                $em->remove($project);

                try
                {
                    $em->flush();
                    $this->addFlash('deleted-ok' , 'Actividad ' . $project . ' eliminada satisfactoriamente.');
                }
                catch (\Exception $e)
                {
                    $this->addFlash('deleted-error' , 'El Actividad ' . $project . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
                }
            }
            else
            {
                $this->addFlash('deleted-error' , 'El Actividad ' . $project . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
            }
        }

        return $this->redirectToRoute('project_index', ['status' => $project->getRealStatus()]);
    }

    /**
     * Search a project entity.
     *
     * @Route("/common/home/search", name="project_home_search")
     * @Method("POST")
     */
    public function homeSearchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $name = $request->get('home-project-search');

        $project = $em->getRepository(Project::class)->findOneBy(['name'=>$name]);

        if ($project != null && ($this->getUser()->getCharge()->getIsProjectManager() || $project->getDesigner()->getId() == $this->getUser()->getId()))
        {
            return $this->redirectToRoute('project_show', ['id' => $project->getId()]);
        }

        $this->addFlash('home-search-error' , '');

        return $this->redirectToRoute('homepage');

    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/back_save/", name="project_back_save")
     * @Method("POST")
     */
    public function backSaveAction(Request $request, Project $project)
    {
        if($project->getRealStatus() == StatusEnum::STATUS_ACTIVATED)
        {
            $em = $this->getDoctrine()->getManager();

            foreach ($project->getStages() as $projectStage) {

                $projectStage->setExecutionStartDate(null);
                $projectStage->setExecutionEndDate(null);

                foreach ($projectStage->getActivities() as $activity)
                {
                    $projectStage->removeActivity($activity);
                    $em->remove($activity);
                }
            }

            foreach ($project->getLogs() as $log)
            {
                $project->removeLog($log);
                $em->remove($log);
            }

            $project->setCurrentStage(1);
            $project->setStatus(StatusEnum::STATUS_SAVED);

            $em->flush();

            $this->addFlash('back-save-ok' , '');

            return $this->redirectToRoute('project_show', array('id' => $project->getId()));
        }

        return $this->redirectToRoute('project_show', array('id' => $project->getId()));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/activate/", name="project_activate")
     * @Method("POST")
     */
    public function activateAction(Request $request, Project $project)
    {
        if($project->getRealStatus() == StatusEnum::STATUS_SAVED || $project->getRealStatus() == StatusEnum::STATUS_FROZEN)
        {
            $projectLog = new ProjectLog();
            $form = $this->createForm(ProjectLogType::class, $projectLog);
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();

            if ($form->isSubmitted() && $form->isValid()) {
                if($project->getCurrentStage() < 1)
                {
                    $project->setCurrentStage(1);
                }
                $project->getCurrentStageObj()->setExecutionStartDate($projectLog->getBeginDate());

                $project->setStatus(StatusEnum::STATUS_ACTIVATED);

                $currentLog = $project->getCurrentLog();
                if($currentLog != null)
                {
                    $currentLog->setIsActive(false);
                    $currentLog->setEndDate($projectLog->getBeginDate());
                }
                $projectLog->setStatus(StatusEnum::STATUS_ACTIVATED);
                $projectLog->setProject($project);
                $projectLog->setIsActive(true);
                $em->persist($projectLog);

                $em->flush();

                $this->addFlash('activated-ok' , '');

                return $this->redirectToRoute('project_execution', array('id' => $project->getId(), 'showStage'=>$project->getCurrentStage()));
            }

            $deleteForm = $this->createDeleteForm($project);

            $logForms = ['activate' => $form->createView(),
                'back' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'approve' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'pay' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'frozen' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'cancel' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView()
            ];

            $this->addFlash('activated-error' , '');

            return $this->render('project/show.html.twig', array(
                'project' => $project,
                'deleteForm' => $deleteForm->createView(),
                'logForms' => $logForms
            ));
        }

        return $this->redirectToRoute('project_show', array('id' => $project->getId()));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/common/{id}/to_review/", name="project_to_review")
     * @Method("POST")
     */
    public function toReviewAction(Request $request, Project $project)
    {
        if (!$this->getUser()->getCharge()->getIsProjectManager() && $this->getUser()->getId() != $project->getDesigner()->getId())
        {
            return $this->redirectToRoute('homepage');
        }

        if($project->getRealStatus() == StatusEnum::STATUS_ACTIVATED)
        {
            $projectLog = new ProjectLog();
            $form = $this->createForm(ProjectLogType::class, $projectLog);
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();

            if ($form->isSubmitted() && $form->isValid()) {

                $project->setStatus(StatusEnum::STATUS_REVIEWED);

                $currentLog = $project->getCurrentLog();
                if($currentLog != null)
                {
                    $currentLog->setIsActive(false);
                    $currentLog->setEndDate($projectLog->getBeginDate());
                }
                $projectLog->setStatus(StatusEnum::STATUS_REVIEWED);
                $projectLog->setProject($project);
                $projectLog->setIsActive(true);
                $em->persist($projectLog);

                $em->flush();

                $this->addFlash('reviewed-ok' , '');

                return $this->redirectToRoute('project_show', array('id' => $project->getId()));
            }

            $this->addFlash('reviewed-error' , '');

            return $this->executionAction($project, $project->getCurrentStage(), $form);
        }

        return $this->redirectToRoute('project_show', array('id' => $project->getId()));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/back_to_execution/", name="project_back_to_execution")
     * @Method("POST")
     */
    public function backToExecutionAction(Request $request, Project $project)
    {
        if($project->getRealStatus() == StatusEnum::STATUS_REVIEWED)
        {
            $projectLog = new ProjectLog();
            $form = $this->createForm(ProjectLogType::class, $projectLog);
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();

            if ($form->isSubmitted() && $form->isValid()) {

                $project->setStatus(StatusEnum::STATUS_ACTIVATED);

                $backStage = $request->get('back-stage');
                $project->setCurrentStage($backStage);
                $project->getCurrentStageObj()->setExecutionStartDate($projectLog->getBeginDate());

                $currentLog = $project->getCurrentLog();
                if($currentLog != null)
                {
                    $currentLog->setIsActive(false);
                    $currentLog->setEndDate($projectLog->getBeginDate());
                }
                $projectLog->setStatus(StatusEnum::STATUS_ACTIVATED);
                $projectLog->setProject($project);
                $projectLog->setIsActive(true);
                $em->persist($projectLog);

                $em->flush();

                $this->addFlash('back-execution-ok' , '');

                return $this->redirectToRoute('project_execution', array('id' => $project->getId(), 'showStage'=>$project->getCurrentStage()));
            }

            $deleteForm = $this->createDeleteForm($project);

            $logForms = ['activate' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'back' => $form->createView(),
                'approve' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'pay' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'frozen' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'cancel' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView()
            ];

            $this->addFlash('back-execution-error' , '');

            return $this->render('project/show.html.twig', array(
                'project' => $project,
                'deleteForm' => $deleteForm->createView(),
                'logForms' => $logForms
            ));
        }

        return $this->redirectToRoute('project_show', array('id' => $project->getId()));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/to_approved/", name="project_to_approved")
     * @Method("POST")
     */
    public function toApprovedAction(Request $request, Project $project)
    {
        if($project->getRealStatus() == StatusEnum::STATUS_REVIEWED)
        {
            $projectLog = new ProjectLog();
            $form = $this->createForm(ProjectLogType::class, $projectLog);
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();

            if ($form->isSubmitted() && $form->isValid()) {

                $project->setStatus(StatusEnum::STATUS_APPROVED);

                $currentLog = $project->getCurrentLog();
                if($currentLog != null)
                {
                    $currentLog->setIsActive(false);
                    $currentLog->setEndDate($projectLog->getBeginDate());
                }
                $projectLog->setStatus(StatusEnum::STATUS_APPROVED);
                $projectLog->setProject($project);
                $projectLog->setIsActive(true);
                $em->persist($projectLog);

                $em->flush();

                $this->addFlash('approved-ok' , '');

                return $this->redirectToRoute('project_show', array('id' => $project->getId()));
            }

            $deleteForm = $this->createDeleteForm($project);

            $logForms = ['activate' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'back' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'approve' => $form->createView(),
                'pay' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'frozen' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'cancel' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView()
            ];

            $this->addFlash('approved-error' , '');

            return $this->render('project/show.html.twig', array(
                'project' => $project,
                'deleteForm' => $deleteForm->createView(),
                'logForms' => $logForms
            ));
        }

        return $this->redirectToRoute('project_show', array('id' => $project->getId()));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/to_quoted/", name="project_to_quoted")
     * @Method("POST")
     */
    public function toQuotedAction(Request $request, Project $project)
    {
        if($project->getRealStatus() == StatusEnum::STATUS_APPROVED || $project->getRealStatus() == StatusEnum::STATUS_CANCELED)
        {
            $projectLog = new ProjectLog();
            $form = $this->createForm(ProjectLogType::class, $projectLog);
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();

            if ($form->isSubmitted() && $form->isValid()) {

                $project->setStatus(StatusEnum::STATUS_QUOTED);

                $currentLog = $project->getCurrentLog();
                if($currentLog != null)
                {
                    $currentLog->setIsActive(false);
                    $currentLog->setEndDate($projectLog->getBeginDate());
                }
                $projectLog->setStatus(StatusEnum::STATUS_QUOTED);
                $projectLog->setProject($project);
                $projectLog->setIsActive(true);
                $em->persist($projectLog);

                $em->flush();

                $this->addFlash('quoted-ok' , '');

                return $this->redirectToRoute('project_show', array('id' => $project->getId()));
            }

            $this->addFlash('quoted-error' , '');

            return $this->quoteAction($project, $form);
        }

        return $this->redirectToRoute('project_show', array('id' => $project->getId()));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/to_paid/", name="project_to_paid")
     * @Method("POST")
     */
    public function toPaidAction(Request $request, Project $project)
    {
        if($project->getRealStatus() == StatusEnum::STATUS_QUOTED)
        {
            $projectLog = new ProjectLog();
            $form = $this->createForm(ProjectLogType::class, $projectLog);
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();

            if ($form->isSubmitted() && $form->isValid()) {

                $project->getPaymentStatus()->setTermName($project->getTerm()->getName());
                $project->getPaymentStatus()->setTermFactor($project->getTerm()->getIncrease());

                $paymentCode = $request->get('payment-code');
                if ($paymentCode != null)
                {
                    $project->getPaymentStatus()->setCode($paymentCode);
                }

                $project->setStatus(StatusEnum::STATUS_PAID);

                $currentLog = $project->getCurrentLog();
                if($currentLog != null)
                {
                    $currentLog->setIsActive(false);
                    $currentLog->setEndDate($projectLog->getBeginDate());
                }
                $projectLog->setStatus(StatusEnum::STATUS_PAID);
                $projectLog->setProject($project);
                $projectLog->setIsActive(true);
                $em->persist($projectLog);

                $em->flush();

                $this->addFlash('paid-ok' , '');

                return $this->redirectToRoute('project_show', array('id' => $project->getId()));
            }

            $deleteForm = $this->createDeleteForm($project);

            $logForms = ['activate' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'back' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'approve' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'pay' => $form->createView(),
                'frozen' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'cancel' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView()
            ];

            $this->addFlash('paid-error' , '');

            return $this->render('project/show.html.twig', array(
                'project' => $project,
                'deleteForm' => $deleteForm->createView(),
                'logForms' => $logForms
            ));
        }

        return $this->redirectToRoute('project_show', array('id' => $project->getId()));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/to_frozen/", name="project_to_frozen")
     * @Method("POST")
     */
    public function toFrozenAction(Request $request, Project $project)
    {
        if($project->getRealStatus() == StatusEnum::STATUS_ACTIVATED)
        {
            $projectLog = new ProjectLog();
            $form = $this->createForm(ProjectLogType::class, $projectLog);
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();

            if ($form->isSubmitted() && $form->isValid()) {

                $project->setStatus(StatusEnum::STATUS_FROZEN);

                $currentLog = $project->getCurrentLog();
                if($currentLog != null)
                {
                    $currentLog->setIsActive(false);
                    $currentLog->setEndDate($projectLog->getBeginDate());
                }
                $projectLog->setStatus(StatusEnum::STATUS_FROZEN);
                $projectLog->setProject($project);
                $projectLog->setIsActive(true);
                $em->persist($projectLog);

                $em->flush();

                $this->addFlash('frozen-ok' , '');

                return $this->redirectToRoute('project_show', array('id' => $project->getId()));
            }

            $deleteForm = $this->createDeleteForm($project);

            $logForms = ['activate' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'back' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'approve' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'pay' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'frozen' => $form->createView(),
                'cancel' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView()
            ];

            $this->addFlash('frozen-error' , '');

            return $this->render('project/show.html.twig', array(
                'project' => $project,
                'deleteForm' => $deleteForm->createView(),
                'logForms' => $logForms
            ));
        }

        return $this->redirectToRoute('project_show', array('id' => $project->getId()));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/to_cancel/", name="project_to_cancel")
     * @Method("POST")
     */
    public function toCancelAction(Request $request, Project $project)
    {
        if($project->getRealStatus() == StatusEnum::STATUS_ACTIVATED || $project->getRealStatus() == StatusEnum::STATUS_FROZEN)
        {
            $projectLog = new ProjectLog();
            $form = $this->createForm(ProjectLogType::class, $projectLog);
            $form->handleRequest($request);
            $em = $this->getDoctrine()->getManager();

            if ($form->isSubmitted() && $form->isValid()) {

                $project->setStatus(StatusEnum::STATUS_CANCELED);

                $currentLog = $project->getCurrentLog();
                if($currentLog != null)
                {
                    $currentLog->setIsActive(false);
                    $currentLog->setEndDate($projectLog->getBeginDate());
                }
                $projectLog->setStatus(StatusEnum::STATUS_CANCELED);
                $projectLog->setProject($project);
                $projectLog->setIsActive(true);
                $em->persist($projectLog);

                $em->flush();

                $this->addFlash('canceled-ok' , '');

                return $this->redirectToRoute('project_show', array('id' => $project->getId()));
            }

            $deleteForm = $this->createDeleteForm($project);

            $logForms = ['activate' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'back' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'approve' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'pay' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'frozen' => $this->createForm(ProjectLogType::class, new ProjectLog())->createView(),
                'cancel' => $form->createView()
            ];

            $this->addFlash('canceled-error' , '');

            return $this->render('project/show.html.twig', array(
                'project' => $project,
                'deleteForm' => $deleteForm->createView(),
                'logForms' => $logForms
            ));
        }

        return $this->redirectToRoute('project_show', array('id' => $project->getId()));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/common/{id}/next_stage/", name="project_next_stage")
     * @Method("POST")
     */
    public function nextStageAction(Request $request, Project $project)
    {
        if(!$this->getUser()->getCharge()->getIsProjectManager())
        {
            if ($this->getUser()->getId() != $project->getDesigner()->getId())
            {
                return $this->redirectToRoute('homepage');
            }

            if ($project->getRealStatus() != StatusEnum::STATUS_ACTIVATED)
            {
                return $this->redirectToRoute('project_execution', ['id' => $project->getId(), 'showStage'=>$project->getCurrentStage()]);
            }
        }
        else
        {
            if($project->getRealStatus() != StatusEnum::STATUS_ACTIVATED && $project->getRealStatus() != StatusEnum::STATUS_APPROVED && $project->getRealStatus() != StatusEnum::STATUS_QUOTED && $project->getRealStatus() != StatusEnum::STATUS_CANCELED)
            {
                return $this->redirectToRoute('project_execution', ['id' => $project->getId(), 'showStage'=>$project->getCurrentStage()]);
            }
        }

        if($project->getCurrentStage() <= count($project->getStages()))
        {
            if($project->getRealStatus() == StatusEnum::STATUS_ACTIVATED || $this->getUser()->getCharge()->getIsProjectManager())
            {
                $oldStage = $project->getCurrentStageObj();
                $form = $this->createForm(NextStageType::class, $oldStage);
                $form->handleRequest($request);

                $project->setCurrentStage($project->getCurrentStage() + 1);

                $currentStage = $project->getCurrentStageObj();
                if ($currentStage != null)
                {
                    $currentStage->setExecutionStartDate(new \DateTime('now'));
                }

                $this->getDoctrine()->getManager()->flush();
            }
        }

        return $this->redirectToRoute('project_execution', array('id' => $project->getId(), 'showStage'=>$project->getCurrentStage()));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/common/{id}/execution/{showStage}/stage/", name="project_execution")
     * @Method("GET")
     */
    public function executionAction(Project $project, $showStage, $toReviewForm = null)
    {
        if (!$this->getUser()->getCharge()->getIsProjectManager() && $this->getUser()->getId() != $project->getDesigner()->getId())
        {
            return $this->redirectToRoute('homepage');
        }

        $currentStage = $project->getCurrentStageObj();
        if($currentStage != null)
        {
            $currentStage->setExecutionEndDate(new \DateTime('now'));
        }
        else
        {
            $currentStage = new ProjectStage();
        }

        $form = $this->createForm(NextStageType::class, $currentStage);

        $labels = [];
        $programationDays = [];
        $executionDays = [];
        $index = 1;

        foreach ($project->getStages() as $stage)
        {
            $labels[] = "Etapa ".$index;
            $index++;

            if($stage->getProgramationStartDate() != null && $stage->getProgramationEndDate() != null)
            {
                $programationDays[] = $this->dateDifference($stage->getProgramationStartDate(), $stage->getProgramationEndDate());
            }
            else
            {
                $programationDays[] = 0;
            }

            if($stage->getExecutionStartDate() != null && $stage->getExecutionEndDate() != null)
            {
                $executionDays[] = $this->dateDifference($stage->getExecutionStartDate(), $stage->getExecutionEndDate());
            }
            else
            {
                $executionDays[] = 0;
            }
        }

        if ($toReviewForm == null)
        {
            $toReviewForm = $this->createForm(ProjectLogType::class, new ProjectLog());
        }

        return $this->render('project/execution.html.twig', array(
            'project' => $project,
            'form' => $form->createView(),
            'toReviewForm' => $toReviewForm->createView(),
            'showStage' => $showStage,
            'chartData' => ['labels' => json_encode($labels),
                            'programation' => json_encode($programationDays),
                            'execution' => json_encode($executionDays)
                           ]
        ));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/quote/", name="project_quote")
     * @Method({"GET", "POST"})
     */
    public function quoteAction(Request $request, Project $project, $toQuotedForm = null)
    {
        $em = $this->getDoctrine()->getManager();

        $needUpdate = false;

        if ($project->getPaymentStatus() == null)
        {
            $paymentStatus = new PaymentStatus();
            $project->setPaymentStatus($paymentStatus);

            $needUpdate = true;
        }

        $paymentStatus = $project->getPaymentStatus();

        foreach ($project->getStages() as $stage)
        {
            foreach ($stage->getActivities() as $activity)
            {
                if(!$paymentStatus->hasActivity($activity))
                {
                    $paymentStatusActivity = new PaymentStatusActivity();
                    $paymentStatusActivity->setActivity($activity);

                    $paymentStatus->addActivity($paymentStatusActivity);

                    $needUpdate = true;
                }
            }
        }

        if ($needUpdate)
        {
            $em->flush();
        }

        $form = $this->createForm(PaymentStatusType::class, $paymentStatus);
        $form->handleRequest($request);


        if ($form->isSubmitted())
        {
            $activitiesComplexity = $request->get('complexity-activity');

            if($activitiesComplexity != null)
            {
                foreach (array_keys($activitiesComplexity) as $activityId)
                {
                    $complexity = $em->getRepository(ActivityComplexity::class)->find($activitiesComplexity[$activityId]);
                    $project->getPaymentStatus()->addActivityComplexity($activityId, $complexity);
                }
            }

            if ($form->isValid())
            {
                $em->flush();

                $this->addFlash('quote-saved-ok' , '');

                return $this->redirectToRoute('project_quote', ['id'=>$project->getId()]);
            }
            else
            {
                $this->addFlash('quote-saved-error' , '');
            }
        }

        if ($toQuotedForm == null)
        {
            $toQuotedForm = $this->createForm(ProjectLogType::class, new ProjectLog());
        }

        $canQuote = true;
        foreach ($project->getPaymentStatus()->getActivities() as $activity)
        {
            if($activity->getComplexity() == null)
            {
                $canQuote = false;
            }
        }

        return $this->render('project/quote.html.twig', array(
            'project' => $project,
            'toQuotedForm' => $toQuotedForm->createView(),
            'form' => $form->createView(),
            'canQuote' => $canQuote

        ));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/quote/excel/worker/{_filename}.{_format}", name="project_quote_excel_worker", requirements={"_format"="csv|xls|xlsx"})
     * @Method({"GET", "POST"})
     */
    public function quoteExcelWorkerAction(Request $request, Project $project, $_format)
    {
        $total = 0;

        $workers = [];

        foreach ($project->getPaymentStatus()->getActivities() as $activity)
        {
            $total += $activity->getPrice();

            $workerId = $activity->getActivity()->getExecutor()->getId();
            if(!isset($workers[$workerId]))
            {
                $workers[$workerId] = ['worker'=>$activity->getActivity()->getExecutor(),
                    'activities'=>[],
                    'total'=>0,
                    ];
            }

            $workers[$workerId]['activities'][] = $activity;
            $workers[$workerId]['total'] += $activity->getPrice();
        }

        return $this->render('project/quote-worker.'.$_format.'.twig', array(
            'project' => $project,
            'workers' => $workers,
            'total' => $total
        ));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/manager/{id}/quote/excel/{_filename}.{_format}", name="project_quote_excel", requirements={"_format"="csv|xls|xlsx"})
     * @Method({"GET", "POST"})
     */
    public function quoteExcelAction(Request $request, Project $project, $_format)
    {
        $totals = ['total'=>0, 'totalFactor'=>0, 'totalBonus'=>0, 'totalTerm'=>0, 'totalPenalty'=>0, 'totalFinal'=>0 ];

        foreach ($project->getPaymentStatus()->getActivities() as $activity)
        {
            $totals['total'] += $activity->getPrice();
        }

        $totals['totalFactor'] = $totals['total'] * $project->getPaymentStatus()->getFactor();
        $totals['totalBonus'] = $totals['total'] * $project->getPaymentStatus()->getBonus();
        $totals['totalPenalty'] = $totals['total'] * $project->getPaymentStatus()->getPenalty();
        if($project->getStatus() == StatusEnum::STATUS_PAID)
        {
            $totals['totalTerm'] = $totals['total'] * $project->getPaymentStatus()->getTermFactor();
        }
        else
        {
            $totals['totalTerm'] = $totals['total'] * $project->getTerm()->getIncrease();
        }

        $totals['totalFinal'] = $totals['total'] + $totals['totalFactor'] + $totals['totalBonus'] + $totals['totalTerm'] - $totals['totalPenalty'];

        return $this->render('project/quote.'.$_format.'.twig', array(
            'project' => $project,
            'totals' => $totals
        ));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/common/{id}/timeline/", name="project_timeline")
     * @Method("GET")
     */
    public function timelineAction(Project $project)
    {
        if (!$this->getUser()->getCharge()->getIsProjectManager() && $this->getUser()->getId() != $project->getDesigner()->getId())
        {
            return $this->redirectToRoute('homepage');
        }

        $statusColor = ['activated'=>'#00B1E1',
            'frozen'=>'#205081',
            'canceled'=>'#E9573F',
            'approved'=>'#21759b',
            'quoted'=>'#906094',
            'paid'=>'#8CC152',
            'reviewed'=>'#F6BB42'
        ];

        $dataSets = [];
        $index = 0;
        foreach ($project->getLogs() as $log) {
            $index++;
            $dataSets[] = ['label'=> $index. ': ' . StatusEnum::getStatusName($log->getStatus()),
                'backgroundColor' => $statusColor[$log->getStatus()],
                'data' => ($log->getEndDate() != null ? $this->dateDifference($log->getBeginDate(), $log->getEndDate()) : 1)
            ];
        }

        //var_dump($dataSets);die();
        return $this->render('project/timeline.html.twig', array(
            'project' => $project,
            'dataSets' => $dataSets
        ));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/common/{id}/execution/stage/{showStage}/activity/{activity_id}/show/", name="project_execution_activity_show")
     * @Method({"GET", "POST"})
     */
    public function activityShowAction(Request $request, Project $project, $showStage, $activity_id)
    {
        if (!$this->getUser()->getCharge()->getIsProjectManager() && $this->getUser()->getId() != $project->getDesigner()->getId())
        {
            return $this->redirectToRoute('homepage');
        }

        $em = $this->getDoctrine()->getManager();

        $activity = $em->getRepository(ProjectStageActivity::class)->find($activity_id);

        return $this->render('project/activity/show.html.twig', array(
            'project' => $project,
            'activity' => $activity,
            'showStage' => $showStage
        ));
    }

    /**
     * Deletes a project activity entity.
     *
     * @Route("/common/{id}/execution/stage/{showStage}/activity/{activity_id}/delete/", name="project_execution_activity_delete")
     * @Method({"GET", "POST"})
     */
    public function activityDeleteAction(Request $request, Project $project, $showStage, $activity_id)
    {
        if(!$this->getUser()->getCharge()->getIsProjectManager())
        {
            if ($this->getUser()->getId() != $project->getDesigner()->getId())
            {
                return $this->redirectToRoute('homepage');
            }

            if ($project->getRealStatus() != StatusEnum::STATUS_ACTIVATED)
            {
                return $this->redirectToRoute('project_execution', ['id' => $project->getId(), 'showStage'=>$showStage]);
            }
        }
        else
        {
            if($project->getRealStatus() != StatusEnum::STATUS_ACTIVATED && $project->getRealStatus() != StatusEnum::STATUS_APPROVED && $project->getRealStatus() != StatusEnum::STATUS_QUOTED && $project->getRealStatus() != StatusEnum::STATUS_CANCELED)
            {
                return $this->redirectToRoute('project_execution', ['id' => $project->getId(), 'showStage'=>$showStage]);
            }
        }

        $em = $this->getDoctrine()->getManager();

        $activity = $em->getRepository(ProjectStageActivity::class)->find($activity_id);

        $em->remove($activity);

        try
        {
            $em->flush();
            $this->addFlash('deleted-activity-ok' , 'Actividad ' . $project . ' eliminada satisfactoriamente.');
        }
        catch (\Exception $e)
        {
            $this->addFlash('deleted-activity-error' , 'El Actividad ' . $project . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
        }

        return $this->redirectToRoute('project_execution', ['id' => $project->getId(), 'showStage'=>$showStage]);
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/common/{id}/execution/stage/{showStage}/activity/new", name="project_execution_activity_new")
     * @Method({"GET", "POST"})
     */
    public function activityNewAction(Request $request, Project $project, $showStage)
    {
        if(!$this->getUser()->getCharge()->getIsProjectManager())
        {
            if ($this->getUser()->getId() != $project->getDesigner()->getId())
            {
                return $this->redirectToRoute('homepage');
            }

            if ($project->getRealStatus() != StatusEnum::STATUS_ACTIVATED)
            {
                return $this->redirectToRoute('project_execution', ['id' => $project->getId(), 'showStage'=>$showStage]);
            }
        }
        else
        {
            if($project->getRealStatus() != StatusEnum::STATUS_ACTIVATED && $project->getRealStatus() != StatusEnum::STATUS_APPROVED && $project->getRealStatus() != StatusEnum::STATUS_QUOTED && $project->getRealStatus() != StatusEnum::STATUS_CANCELED)
            {
                return $this->redirectToRoute('project_execution', ['id' => $project->getId(), 'showStage'=>$showStage]);
            }
        }

        $activity = new ProjectStageActivity();
        $showStageObj = $project->getStageByOrder($showStage);
        $activity->setProjectStage($showStageObj);
        $activity->setStartDate(new \DateTime('now'));
        $activity->setEndDate(new \DateTime('now'));

        $form = $this->createForm(ProjectStageActivityType::class, $activity, ['project'=>$project, 'stage'=>$showStageObj->getStage()]);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($activity);
            $em->flush();

            $this->addFlash('created-ok' , '');

            $submitButton = $request->get('submit-button');
            if ($submitButton == 'create')
            {
                return $this->redirectToRoute('project_execution', array('id' => $project->getId(), 'showStage'=>$showStage));
            }

            return $this->redirectToRoute('project_execution_activity_new', array('id' => $project->getId(), 'showStage'=>$showStage));
        }

        return $this->render('project/activity/new.html.twig', array(
            'project' => $project,
            'form' => $form->createView(),
            'showStage' => $showStage
        ));
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/common/{id}/execution/stage/{showStage}/activity/{activity_id}/edit/", name="project_execution_activity_edit")
     * @Method({"GET", "POST"})
     */
    public function activityEditAction(Request $request, Project $project, $showStage, $activity_id)
    {
        if(!$this->getUser()->getCharge()->getIsProjectManager())
        {
            if ($this->getUser()->getId() != $project->getDesigner()->getId())
            {
                return $this->redirectToRoute('homepage');
            }

            if ($project->getRealStatus() != StatusEnum::STATUS_ACTIVATED)
            {
                return $this->redirectToRoute('project_execution', ['id' => $project->getId(), 'showStage'=>$showStage]);
            }
        }
        else
        {
            if($project->getRealStatus() != StatusEnum::STATUS_ACTIVATED && $project->getRealStatus() != StatusEnum::STATUS_APPROVED && $project->getRealStatus() != StatusEnum::STATUS_QUOTED && $project->getRealStatus() != StatusEnum::STATUS_CANCELED)
            {
                return $this->redirectToRoute('project_execution', ['id' => $project->getId(), 'showStage'=>$showStage]);
            }
        }


        $em = $this->getDoctrine()->getManager();

        $activity = $em->getRepository(ProjectStageActivity::class)->find($activity_id);

        $form = $this->createForm(ProjectStageActivityType::class, $activity, ['project'=>$project, 'stage'=>$activity->getProjectStage()->getStage()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($activity);
            $em->flush();

            $this->addFlash('edited-ok' , '');

            return $this->redirectToRoute('project_execution_activity_edit', array('id' => $project->getId(), 'showStage'=>$showStage, 'activity_id'=>$activity->getId()));
        }

        return $this->render('project/activity/edit.html.twig', array(
            'project' => $project,
            'form' => $form->createView(),
            'showStage' => $showStage
        ));
    }

    /**
     * Creates a form to delete a project entity.
     *
     * @param Project $project The project entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Project $project)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('project_delete', array('id' => $project->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

    private function dateDifference($datetime1 , $datetime2 , $differenceFormat = '%a' )
    {
        $interval = date_diff($datetime1, $datetime2);

        return $interval->format($differenceFormat) + 1;
    }
}
