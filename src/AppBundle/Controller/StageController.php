<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Stage;
use AppBundle\Form\StageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Stage controller.
 *
 * @Route("/manager/stage")
 */
class StageController extends Controller
{
    /**
     * Lists all stage entities.
     *
     * @Route("/", name="stage_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $stages = $em->getRepository(Stage::class)->findAll();

        $deleteForms = array();
        foreach ($stages as $stage)
        {
            $deleteForms[$stage->getId()] = $this->createDeleteForm($stage)->createView();
        }

        return $this->render('stage/index.html.twig', array(
            'stages' => $stages,
            'deleteForms' => $deleteForms
        ));
    }

    /**
     * Creates a new stage entity.
     *
     * @Route("/new/", name="stage_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $stage = new Stage();
        $form = $this->createForm(StageType::class, $stage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($stage);
            $em->flush();

            $this->addFlash('created-ok' , 'Etapa creado satisfactoriamente.');

            $submitButton = $request->get('submit-button');
            if ($submitButton == 'create')
            {
                return $this->redirectToRoute('stage_show', array('id' => $stage->getId()));
            }
            return $this->redirectToRoute('stage_new');
        }

        return $this->render('stage/new.html.twig', array(
            'stage' => $stage,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a stage entity.
     *
     * @Route("/{id}/", name="stage_show")
     * @Method("GET")
     */
    public function showAction(Stage $stage)
    {
        $deleteForm = $this->createDeleteForm($stage);

        return $this->render('stage/show.html.twig', array(
            'stage' => $stage,
            'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing stage entity.
     *
     * @Route("/{id}/edit/", name="stage_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Stage $stage)
    {
        $editForm = $this->createForm(StageType::class, $stage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('edited-ok' , 'Etapa editado satisfactoriamente.');

            return $this->redirectToRoute('stage_edit', array('id' => $stage->getId()));
        }

        return $this->render('stage/edit.html.twig', array(
            'stage' => $stage,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a stage entity.
     *
     * @Route("/{id}/", name="stage_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Stage $stage)
    {
        $form = $this->createDeleteForm($stage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($stage);

            try
            {
                $em->flush();
                $this->addFlash('deleted-ok' , 'Etapa ' . $stage . ' eliminada satisfactoriamente.');
            }
            catch (\Exception $e)
            {
                $this->addFlash('deleted-error' , 'El Etapa ' . $stage . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
            }
        }

        return $this->redirectToRoute('stage_index');
    }

    /**
     * Creates a form to delete a stage entity.
     *
     * @param Stage $stage The stage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Stage $stage)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('stage_delete', array('id' => $stage->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
