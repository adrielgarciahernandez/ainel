<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TypeOT;
use AppBundle\Form\TypeOTType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * TypeOT controller.
 *
 * @Route("/manager/typeOT")
 */
class TypeOTController extends Controller
{
    /**
     * Lists all typeOT entities.
     *
     * @Route("/", name="typeOT_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $typeOTs = $em->getRepository(TypeOT::class)->findAll();

        $deleteForms = array();
        foreach ($typeOTs as $typeOT)
        {
            $deleteForms[$typeOT->getId()] = $this->createDeleteForm($typeOT)->createView();
        }

        return $this->render('typeOT/index.html.twig', array(
            'typeOTs' => $typeOTs,
            'deleteForms' => $deleteForms
        ));
    }

    /**
     * Creates a new typeOT entity.
     *
     * @Route("/new/", name="typeOT_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $typeOT = new TypeOT();
        $form = $this->createForm(TypeOTType::class, $typeOT);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeOT);
            $em->flush();

            $this->addFlash('created-ok' , 'Actividad creado satisfactoriamente.');

            $submitButton = $request->get('submit-button');
            if ($submitButton == 'create')
            {
                return $this->redirectToRoute('typeOT_show', array('id' => $typeOT->getId()));
            }
            return $this->redirectToRoute('typeOT_new');
        }

        return $this->render('typeOT/new.html.twig', array(
            'typeOT' => $typeOT,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a typeOT entity.
     *
     * @Route("/{id}/", name="typeOT_show")
     * @Method("GET")
     */
    public function showAction(TypeOT $typeOT)
    {
        $deleteForm = $this->createDeleteForm($typeOT);

        return $this->render('typeOT/show.html.twig', array(
            'typeOT' => $typeOT,
            'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing typeOT entity.
     *
     * @Route("/{id}/edit/", name="typeOT_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, TypeOT $typeOT)
    {
        $editForm = $this->createForm(TypeOTType::class, $typeOT);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('edited-ok' , 'Actividad editado satisfactoriamente.');

            return $this->redirectToRoute('typeOT_edit', array('id' => $typeOT->getId()));
        }

        return $this->render('typeOT/edit.html.twig', array(
            'typeOT' => $typeOT,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a typeOT entity.
     *
     * @Route("/{id}/", name="typeOT_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TypeOT $typeOT)
    {
        $form = $this->createDeleteForm($typeOT);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeOT);

            try
            {
                $em->flush();
                $this->addFlash('deleted-ok' , 'Actividad ' . $typeOT . ' eliminada satisfactoriamente.');
            }
            catch (\Exception $e)
            {
                $this->addFlash('deleted-error' , 'El Actividad ' . $typeOT . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
            }
        }

        return $this->redirectToRoute('typeOT_index');
    }

    /**
     * Creates a form to delete a typeOT entity.
     *
     * @param TypeOT $typeOT The typeOT entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TypeOT $typeOT)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('typeOT_delete', array('id' => $typeOT->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
