<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Area;
use AppBundle\Form\AreaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Area controller.
 *
 * @Route("/manager/area")
 */
class AreaController extends Controller
{
    /**
     * Lists all area entities.
     *
     * @Route("/", name="area_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $areas = $em->getRepository(Area::class)->findAll();

        $deleteForms = array();
        foreach ($areas as $area)
        {
            $deleteForms[$area->getId()] = $this->createDeleteForm($area)->createView();
        }

        return $this->render('area/index.html.twig', array(
            'areas' => $areas,
            'deleteForms' => $deleteForms
        ));
    }

    /**
     * Creates a new area entity.
     *
     * @Route("/new/", name="area_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $area = new Area();
        $form = $this->createForm(AreaType::class, $area);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($area);
            $em->flush();

            $this->addFlash('created-ok' , 'Actividad creado satisfactoriamente.');

            $submitButton = $request->get('submit-button');
            if ($submitButton == 'create')
            {
                return $this->redirectToRoute('area_show', array('id' => $area->getId()));
            }
            return $this->redirectToRoute('area_new');
        }

        return $this->render('area/new.html.twig', array(
            'area' => $area,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a area entity.
     *
     * @Route("/{id}/", name="area_show")
     * @Method("GET")
     */
    public function showAction(Area $area)
    {
        $deleteForm = $this->createDeleteForm($area);

        return $this->render('area/show.html.twig', array(
            'area' => $area,
            'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing area entity.
     *
     * @Route("/{id}/edit/", name="area_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Area $area)
    {
        $editForm = $this->createForm(AreaType::class, $area);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('edited-ok' , 'Actividad editado satisfactoriamente.');

            return $this->redirectToRoute('area_edit', array('id' => $area->getId()));
        }

        return $this->render('area/edit.html.twig', array(
            'area' => $area,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a area entity.
     *
     * @Route("/{id}/", name="area_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Area $area)
    {
        $form = $this->createDeleteForm($area);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($area);

            try
            {
                $em->flush();
                $this->addFlash('deleted-ok' , 'Actividad ' . $area . ' eliminada satisfactoriamente.');
            }
            catch (\Exception $e)
            {
                $this->addFlash('deleted-error' , 'El Actividad ' . $area . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
            }
        }

        return $this->redirectToRoute('area_index');
    }

    /**
     * Creates a form to delete a area entity.
     *
     * @param Area $area The area entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Area $area)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('area_delete', array('id' => $area->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
