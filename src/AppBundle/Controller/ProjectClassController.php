<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ProjectClass;
use AppBundle\Form\ProjectClassType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * ProjectClass controller.
 *
 * @Route("/manager/projectClass")
 */
class ProjectClassController extends Controller
{
    /**
     * Lists all projectClass entities.
     *
     * @Route("/", name="projectClass_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $projectClasss = $em->getRepository(ProjectClass::class)->findAll();

        $deleteForms = array();
        foreach ($projectClasss as $projectClass)
        {
            $deleteForms[$projectClass->getId()] = $this->createDeleteForm($projectClass)->createView();
        }

        return $this->render('projectClass/index.html.twig', array(
            'projectClasss' => $projectClasss,
            'deleteForms' => $deleteForms
        ));
    }

    /**
     * Creates a new projectClass entity.
     *
     * @Route("/new/", name="projectClass_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $projectClass = new ProjectClass();
        $form = $this->createForm(ProjectClassType::class, $projectClass);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectClass);
            $em->flush();

            $this->addFlash('created-ok' , 'Actividad creado satisfactoriamente.');

            $submitButton = $request->get('submit-button');
            if ($submitButton == 'create')
            {
                return $this->redirectToRoute('projectClass_show', array('id' => $projectClass->getId()));
            }
            return $this->redirectToRoute('projectClass_new');
        }

        return $this->render('projectClass/new.html.twig', array(
            'projectClass' => $projectClass,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a projectClass entity.
     *
     * @Route("/{id}/", name="projectClass_show")
     * @Method("GET")
     */
    public function showAction(ProjectClass $projectClass)
    {
        $deleteForm = $this->createDeleteForm($projectClass);

        return $this->render('projectClass/show.html.twig', array(
            'projectClass' => $projectClass,
            'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing projectClass entity.
     *
     * @Route("/{id}/edit/", name="projectClass_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ProjectClass $projectClass)
    {
        $editForm = $this->createForm(ProjectClassType::class, $projectClass);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('edited-ok' , 'Actividad editado satisfactoriamente.');

            return $this->redirectToRoute('projectClass_edit', array('id' => $projectClass->getId()));
        }

        return $this->render('projectClass/edit.html.twig', array(
            'projectClass' => $projectClass,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a projectClass entity.
     *
     * @Route("/{id}/", name="projectClass_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ProjectClass $projectClass)
    {
        $form = $this->createDeleteForm($projectClass);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($projectClass);

            try
            {
                $em->flush();
                $this->addFlash('deleted-ok' , 'Actividad ' . $projectClass . ' eliminada satisfactoriamente.');
            }
            catch (\Exception $e)
            {
                $this->addFlash('deleted-error' , 'El Actividad ' . $projectClass . ' no puede ser eliminada. Está siendo utilizado por el sistema.');
            }
        }

        return $this->redirectToRoute('projectClass_index');
    }

    /**
     * Creates a form to delete a projectClass entity.
     *
     * @param ProjectClass $projectClass The projectClass entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProjectClass $projectClass)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('projectClass_delete', array('id' => $projectClass->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
