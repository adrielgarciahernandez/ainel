<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserEditType;
use AppBundle\Form\UserPasswordType;
use AppBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * Lists all user entities.
     *
     * @Route("/manager/", name="user_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository(User::class)->findAll();

        $deleteForms = array();
        foreach ($users as $user)
        {
            $deleteForms[$user->getId()] = $this->createDeleteForm($user)->createView();
        }

        return $this->render('user/index.html.twig', array(
            'users' => $users,
            'deleteForms' => $deleteForms
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/manager/new/", name="user_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setEnabled(true);
            $user->setUsername($user->getEmail());

            $roles = [];
            if ($user->getCharge()->getIsProjectManager())
            {
                $roles[] = 'ROLE_PROJECT_MANAGER';
            }
            if ($user->getCharge()->getIsProjectDirector())
            {
                $roles[] = 'ROLE_PROJECT_DIRECTOR';
            }
            $user->setRoles($roles);

            $userManager->updateUser($user);

            $this->addFlash('created-ok' , 'Cargo creado satisfactoriamente.');

            $submitButton = $request->get('submit-button');
            if ($submitButton == 'create')
            {
                return $this->redirectToRoute('user_show', array('id' => $user->getId()));
            }
            return $this->redirectToRoute('user_new');
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a user entity.
     *
     * @Route("/common/{id}/", name="user_show")
     * @Method("GET")
     */
    public function showAction(User $user)
    {
        if(!$this->getUser()->getCharge()->getIsProjectManager() && $user->getId() != $this->getUser()->getId())
        {
            return $this->redirectToRoute('homepage');
        }

        $deleteForm = $this->createDeleteForm($user);

        return $this->render('user/show.html.twig', array(
            'user' => $user,
            'deleteForm' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/manager/{id}/edit/", name="user_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, User $user)
    {
        $editForm = $this->createForm(UserEditType::class, $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $user->setUsername($user->getEmail());
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('edited-ok' , 'Cargo editado satisfactoriamente.');

            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }

        return $this->render('user/edit.html.twig', array(
            'user' => $user,
            'form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/common/password/{id}/", name="user_password")
     * @Method({"GET", "POST"})
     */
    public function passwordAction(Request $request, User $user)
    {
        if(!$this->getUser()->getCharge()->getIsProjectManager() && $user->getId() != $this->getUser()->getId())
        {
            return $this->redirectToRoute('homepage');
        }

        $userManager = $this->container->get('fos_user.user_manager');

        $form = $this->createForm(UserPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $userManager->updateUser($user);

            $this->addFlash('password-ok' , 'Contraseña cambiada satisfactoriamente.');

            return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }

        return $this->render('user/password.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/manager/{id}/", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);

            try
            {
                $em->flush();
                $this->addFlash('deleted-ok' , 'Cargo ' . $user . ' eliminado satisfactoriamente.');
            }
            catch (\Exception $e)
            {
                $this->addFlash('deleted-error' , 'El Cargo ' . $user . ' no puede ser eliminado. Está siendo utilizado por el sistema.');
            }
        }

        return $this->redirectToRoute('user_index');
    }

    /**
     * Creates a form to delete a user entity.
     *
     * @param User $user The user entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
