<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="project_stage_activity")
 */
class ProjectStageActivity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProjectStage", inversedBy="activities")
     * @ORM\JoinColumn(name="projectStage_id", referencedColumnName="id")
     */
    private $projectStage;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Activity")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     */
    private $activity;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="executor_id", referencedColumnName="id")
     */
    private $executor;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="date")
     */
    private $startDate;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="date")
     */
    private $endDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isProgrammed;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @Assert\Type(
     *     type="integer",
     *     message="El valor {{ value }} no es un número entero"
     * )
     * @Assert\GreaterThanOrEqual(
     *     value = 1, 
     *     message="La cantidad debe ser mayor que 1"
     * )
     * @ORM\Column(type="integer")
     */
    private $count;

    public function __construct()
    {
        $this->isProgrammed = false;
        $this->count = 1;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return ProjectStageActivity
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return ProjectStageActivity
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set projectStage
     *
     * @param \AppBundle\Entity\ProjectStage $projectStage
     *
     * @return ProjectStageActivity
     */
    public function setProjectStage(\AppBundle\Entity\ProjectStage $projectStage = null)
    {
        $this->projectStage = $projectStage;

        return $this;
    }

    /**
     * Get projectStage
     *
     * @return \AppBundle\Entity\ProjectStage
     */
    public function getProjectStage()
    {
        return $this->projectStage;
    }

    /**
     * Set executor
     *
     * @param \AppBundle\Entity\User $executor
     *
     * @return ProjectStageActivity
     */
    public function setExecutor(\AppBundle\Entity\User $executor = null)
    {
        $this->executor = $executor;

        return $this;
    }

    /**
     * Get executor
     *
     * @return \AppBundle\Entity\User
     */
    public function getExecutor()
    {
        return $this->executor;
    }

    /**
     * Set activity
     *
     * @param \AppBundle\Entity\Activity $activity
     *
     * @return ProjectStageActivity
     */
    public function setActivity(\AppBundle\Entity\Activity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \AppBundle\Entity\Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProjectStageActivity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isProgrammed
     *
     * @param boolean $isProgrammed
     *
     * @return ProjectStageActivity
     */
    public function setIsProgrammed($isProgrammed)
    {
        $this->isProgrammed = $isProgrammed;

        return $this;
    }

    /**
     * Get isProgrammed
     *
     * @return boolean
     */
    public function getIsProgrammed()
    {
        return $this->isProgrammed;
    }

    /**
     * Set count
     *
     * @param integer $count
     *
     * @return ProjectStageActivity
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer
     */
    public function getCount()
    {
        return $this->count;
    }
}
