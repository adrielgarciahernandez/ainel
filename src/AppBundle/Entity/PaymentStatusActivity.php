<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="payment_status_activity")
 */
class PaymentStatusActivity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\PaymentStatus", inversedBy="activities")
     * @ORM\JoinColumn(name="paymentStatus_id", referencedColumnName="id")
     */
    private $paymentStatus;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProjectStageActivity")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     */
    private $activity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ActivityComplexity")
     * @ORM\JoinColumn(name="complexity_id", referencedColumnName="id")
     */
    private $complexity;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    public function __construct()
    {
        $this->price = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return PaymentStatusActivity
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set paymentStatus
     *
     * @param \AppBundle\Entity\PaymentStatus $paymentStatus
     *
     * @return PaymentStatusActivity
     */
    public function setPaymentStatus(\AppBundle\Entity\PaymentStatus $paymentStatus = null)
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return \AppBundle\Entity\PaymentStatus
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * Set activity
     *
     * @param \AppBundle\Entity\ProjectStageActivity $activity
     *
     * @return PaymentStatusActivity
     */
    public function setActivity(\AppBundle\Entity\ProjectStageActivity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \AppBundle\Entity\ProjectStageActivity
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set complexity
     *
     * @param \AppBundle\Entity\ActivityComplexity $complexity
     *
     * @return PaymentStatusActivity
     */
    public function setComplexity(\AppBundle\Entity\ActivityComplexity $complexity = null)
    {
        $this->complexity = $complexity;

        return $this;
    }

    /**
     * Get complexity
     *
     * @return \AppBundle\Entity\ActivityComplexity
     */
    public function getComplexity()
    {
        return $this->complexity;
    }
}
