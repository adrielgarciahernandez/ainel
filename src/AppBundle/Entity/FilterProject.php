<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FilterProjectRepository")
 * @ORM\Table(name="filter_project")
 */
class FilterProject
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=300)
     */
    private $address;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Comuna")
     * @ORM\JoinTable(name="filter_comuna",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="comuna_id", referencedColumnName="id")}
     *      )
     */
    private $comuna;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Area")
     * @ORM\JoinTable(name="filter_area",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="area_id", referencedColumnName="id")}
     *      )
     */
    private $area;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\ProjectKind")
     * @ORM\JoinTable(name="filter_kind",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="kind_id", referencedColumnName="id")}
     *      )
     */
    private $kind;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\ProjectClass")
     * @ORM\JoinTable(name="filter_class",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="class_id", referencedColumnName="id")}
     *      )
     */
    private $class;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Term")
     * @ORM\JoinTable(name="filter_term",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="term_id", referencedColumnName="id")}
     *      )
     */
    private $term;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinTable(name="filter_designer",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="designer_id", referencedColumnName="id")}
     *      )
     */
    private $designer;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinTable(name="filter_executive",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="executive_id", referencedColumnName="id")}
     *      )
     */
    private $executive;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinTable(name="filter_specialist",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="specialist_id", referencedColumnName="id")}
     *      )
     */
    private $specialist;

    /**
     * @ORM\Column(type="date")
     */
    private $applicationDateBegin;

    /**
     * @ORM\Column(type="date")
     */
    private $applicationDateEnd;

    /**
     * @ORM\Column(type="date")
     */
    private $deliveryDateBegin;

    /**
     * @ORM\Column(type="date")
     */
    private $deliveryDateEnd;

    /**
     * @ORM\Column(type="array")
     */
    private $status;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comuna = new ArrayCollection();
        $this->area = new ArrayCollection();
        $this->kind = new ArrayCollection();
        $this->class = new ArrayCollection();
        $this->term = new ArrayCollection();

        $this->designer = new ArrayCollection();
        $this->executive = new ArrayCollection();
        $this->specialist = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return FilterProject
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return FilterProject
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set applicationDateBegin
     *
     * @param \DateTime $applicationDateBegin
     *
     * @return FilterProject
     */
    public function setApplicationDateBegin($applicationDateBegin)
    {
        $this->applicationDateBegin = $applicationDateBegin;

        return $this;
    }

    /**
     * Get applicationDateBegin
     *
     * @return \DateTime
     */
    public function getApplicationDateBegin()
    {
        return $this->applicationDateBegin;
    }

    /**
     * Set applicationDateEnd
     *
     * @param \DateTime $applicationDateEnd
     *
     * @return FilterProject
     */
    public function setApplicationDateEnd($applicationDateEnd)
    {
        $this->applicationDateEnd = $applicationDateEnd;

        return $this;
    }

    /**
     * Get applicationDateEnd
     *
     * @return \DateTime
     */
    public function getApplicationDateEnd()
    {
        return $this->applicationDateEnd;
    }

    /**
     * Set deliveryDateBegin
     *
     * @param \DateTime $deliveryDateBegin
     *
     * @return FilterProject
     */
    public function setDeliveryDateBegin($deliveryDateBegin)
    {
        $this->deliveryDateBegin = $deliveryDateBegin;

        return $this;
    }

    /**
     * Get deliveryDateBegin
     *
     * @return \DateTime
     */
    public function getDeliveryDateBegin()
    {
        return $this->deliveryDateBegin;
    }

    /**
     * Set deliveryDateEnd
     *
     * @param \DateTime $deliveryDateEnd
     *
     * @return FilterProject
     */
    public function setDeliveryDateEnd($deliveryDateEnd)
    {
        $this->deliveryDateEnd = $deliveryDateEnd;

        return $this;
    }

    /**
     * Get deliveryDateEnd
     *
     * @return \DateTime
     */
    public function getDeliveryDateEnd()
    {
        return $this->deliveryDateEnd;
    }

    /**
     * Set status
     *
     * @param array $status
     *
     * @return FilterProject
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return array
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add comuna
     *
     * @param \AppBundle\Entity\Comuna $comuna
     *
     * @return FilterProject
     */
    public function addComuna(\AppBundle\Entity\Comuna $comuna)
    {
        $this->comuna[] = $comuna;

        return $this;
    }

    /**
     * Remove comuna
     *
     * @param \AppBundle\Entity\Comuna $comuna
     */
    public function removeComuna(\AppBundle\Entity\Comuna $comuna)
    {
        $this->comuna->removeElement($comuna);
    }

    /**
     * Get comuna
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComuna()
    {
        return $this->comuna;
    }

    /**
     * Add area
     *
     * @param \AppBundle\Entity\Area $area
     *
     * @return FilterProject
     */
    public function addArea(\AppBundle\Entity\Area $area)
    {
        $this->area[] = $area;

        return $this;
    }

    /**
     * Remove area
     *
     * @param \AppBundle\Entity\Area $area
     */
    public function removeArea(\AppBundle\Entity\Area $area)
    {
        $this->area->removeElement($area);
    }

    /**
     * Get area
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Add kind
     *
     * @param \AppBundle\Entity\ProjectKind $kind
     *
     * @return FilterProject
     */
    public function addKind(\AppBundle\Entity\ProjectKind $kind)
    {
        $this->kind[] = $kind;

        return $this;
    }

    /**
     * Remove kind
     *
     * @param \AppBundle\Entity\ProjectKind $kind
     */
    public function removeKind(\AppBundle\Entity\ProjectKind $kind)
    {
        $this->kind->removeElement($kind);
    }

    /**
     * Get kind
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Add class
     *
     * @param \AppBundle\Entity\ProjectClass $class
     *
     * @return FilterProject
     */
    public function addClass(\AppBundle\Entity\ProjectClass $class)
    {
        $this->class[] = $class;

        return $this;
    }

    /**
     * Remove class
     *
     * @param \AppBundle\Entity\ProjectClass $class
     */
    public function removeClass(\AppBundle\Entity\ProjectClass $class)
    {
        $this->class->removeElement($class);
    }

    /**
     * Get class
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Add term
     *
     * @param \AppBundle\Entity\Term $term
     *
     * @return FilterProject
     */
    public function addTerm(\AppBundle\Entity\Term $term)
    {
        $this->term[] = $term;

        return $this;
    }

    /**
     * Remove term
     *
     * @param \AppBundle\Entity\Term $term
     */
    public function removeTerm(\AppBundle\Entity\Term $term)
    {
        $this->term->removeElement($term);
    }

    /**
     * Get term
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Add designer
     *
     * @param \AppBundle\Entity\User $designer
     *
     * @return FilterProject
     */
    public function addDesigner(\AppBundle\Entity\User $designer)
    {
        $this->designer[] = $designer;

        return $this;
    }

    /**
     * Remove designer
     *
     * @param \AppBundle\Entity\User $designer
     */
    public function removeDesigner(\AppBundle\Entity\User $designer)
    {
        $this->designer->removeElement($designer);
    }

    /**
     * Get designer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesigner()
    {
        return $this->designer;
    }

    /**
     * Add executive
     *
     * @param \AppBundle\Entity\User $executive
     *
     * @return FilterProject
     */
    public function addExecutive(\AppBundle\Entity\User $executive)
    {
        $this->executive[] = $executive;

        return $this;
    }

    /**
     * Remove executive
     *
     * @param \AppBundle\Entity\User $executive
     */
    public function removeExecutive(\AppBundle\Entity\User $executive)
    {
        $this->executive->removeElement($executive);
    }

    /**
     * Get executive
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExecutive()
    {
        return $this->executive;
    }

    /**
     * Add specialist
     *
     * @param \AppBundle\Entity\User $specialist
     *
     * @return FilterProject
     */
    public function addSpecialist(\AppBundle\Entity\User $specialist)
    {
        $this->specialist[] = $specialist;

        return $this;
    }

    /**
     * Remove specialist
     *
     * @param \AppBundle\Entity\User $specialist
     */
    public function removeSpecialist(\AppBundle\Entity\User $specialist)
    {
        $this->specialist->removeElement($specialist);
    }

    /**
     * Get specialist
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialist()
    {
        return $this->specialist;
    }
}
