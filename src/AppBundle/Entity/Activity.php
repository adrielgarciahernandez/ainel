<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @UniqueEntity(
 *     fields={"name"},
 *     errorPath="name",
 *     message="Ya existe una Actividad con ese nombre."
 * )
 * @ORM\Entity
 * @ORM\Table(name="activity")
 */
class Activity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Stage", inversedBy="activities")
     * @ORM\JoinColumn(name="stage_id", referencedColumnName="id")
     */
    private $stage;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @Assert\Valid()
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ActivityComplexityValue", mappedBy="activity", cascade={"persist"})
     */
    private $values;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Activity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param boolean $description
     *
     * @return Activity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return boolean
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Set code
     *
     * @param string $code
     *
     * @return Activity
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set stage
     *
     * @param \AppBundle\Entity\Stage $stage
     *
     * @return Activity
     */
    public function setStage(\AppBundle\Entity\Stage $stage = null)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return \AppBundle\Entity\Stage
     */
    public function getStage()
    {
        return $this->stage;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->values = new ArrayCollection();
    }

    /**
     * Add value
     *
     * @param \AppBundle\Entity\ActivityComplexityValue $value
     *
     * @return Activity
     */
    public function addValue(\AppBundle\Entity\ActivityComplexityValue $value)
    {
        $this->values[] = $value;

        $value->setActivity($this);

        return $this;
    }

    public function hasValue($complexity)
    {
        foreach ($this->values as $value)
        {
            if($value->getComplexity()->getId() == $complexity->getId())
            {
                return true;
            }
        }

        return false;
    }

    public function getComplexityValue($complexity)
    {
        if($complexity == null)
        {
            return 0;
        }

        foreach ($this->values as $value)
        {
            if($value->getComplexity()->getId() == $complexity->getId())
            {
                return $value->getValue();
            }
        }

        return 0;
    }

    /**
     * Remove value
     *
     * @param \AppBundle\Entity\ActivityComplexityValue $value
     */
    public function removeValue(\AppBundle\Entity\ActivityComplexityValue $value)
    {
        $this->values->removeElement($value);
    }

    /**
     * Get values
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValues()
    {
        return $this->values;
    }
}
