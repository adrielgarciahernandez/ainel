<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
/**
 * @UniqueEntity(
 *     fields={"ot", "otNumber", "modification"},
 *     errorPath="ot",
 *     message="Ya existe un Proyecto, con este nombre y esta modificación.",
 * )
 * @ORM\Entity
 * @ORM\Table(name="project")
 */
class Project
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TypeOT")
     * @ORM\JoinColumn(name="ot_id", referencedColumnName="id")
     */
    private $ot;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="integer")
     */
    private $otNumber;

    /**
     * @ORM\Column(type="string")
     */
    private $modification;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="string", length=300)
     */
    private $address;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Comuna")
     * @ORM\JoinColumn(name="comuna_id", referencedColumnName="id")
     */
    private $comuna;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Area")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    private $area;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\ProjectKind")
     * @ORM\JoinTable(name="project_kind",
     *      joinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="kind_id", referencedColumnName="id")}
     *      )
     */
    private $kind;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProjectClass")
     * @ORM\JoinColumn(name="class_id", referencedColumnName="id")
     */
    private $class;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Term")
     * @ORM\JoinColumn(name="term_id", referencedColumnName="id")
     */
    private $term;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProjectType")
     * @ORM\JoinColumn(name="methodology_id", referencedColumnName="id")
     */
    private $methodology;

    /**
     * @ORM\Column(type="boolean")
     */
    private $useMethodology;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="specialist_id", referencedColumnName="id")
     */
    private $specialist;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="executive_id", referencedColumnName="id")
     */
    private $executive;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(name="designer_id", referencedColumnName="id")
     */
    private $designer;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="date")
     */
    private $applicationDate;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="date")
     */
    private $deliveryDate;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProjectStage", mappedBy="project", cascade={"persist"})
     */
    private $stages;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProjectLog", mappedBy="project", cascade={"persist"})
     */
    private $logs;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $currentStage;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\PaymentStatus", mappedBy="project", cascade={"persist"})
     */
    private $paymentStatus;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param boolean $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return boolean
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set otNumber
     *
     * @param integer $otNumber
     *
     * @return Project
     */
    public function setOtNumber($otNumber)
    {
        $this->otNumber = $otNumber;

        return $this;
    }

    /**
     * Get otNumber
     *
     * @return integer
     */
    public function getOtNumber()
    {
        return $this->otNumber;
    }

    /**
     * Set modification
     *
     * @param integer $modification
     *
     * @return Project
     */
    public function setModification($modification)
    {
        $this->modification = $modification;

        if ($this->modification == null)
        {
            $this->modification = 'none';
        }

        return $this;
    }

    /**
     * Get modification
     *
     * @return integer
     */
    public function getModification()
    {
        if ($this->modification == 'none')
        {
            return null;
        }

        return $this->modification;
    }
    
    /**
     * Set address
     *
     * @param string $address
     *
     * @return Project
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set applicationDate
     *
     * @param \DateTime $applicationDate
     *
     * @return Project
     */
    public function setApplicationDate($applicationDate)
    {
        $this->applicationDate = $applicationDate;

        return $this;
    }

    /**
     * Get applicationDate
     *
     * @return \DateTime
     */
    public function getApplicationDate()
    {
        return $this->applicationDate;
    }

    /**
     * Set deliveryDate
     *
     * @param \DateTime $deliveryDate
     *
     * @return Project
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    /**
     * Get deliveryDate
     *
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * Set ot
     *
     * @param \AppBundle\Entity\TypeOT $ot
     *
     * @return Project
     */
    public function setOt(\AppBundle\Entity\TypeOT $ot = null)
    {
        $this->ot = $ot;

        return $this;
    }

    /**
     * Get ot
     *
     * @return \AppBundle\Entity\TypeOT
     */
    public function getOt()
    {
        return $this->ot;
    }

    /**
     * Set comuna
     *
     * @param \AppBundle\Entity\Comuna $comuna
     *
     * @return Project
     */
    public function setComuna(\AppBundle\Entity\Comuna $comuna = null)
    {
        $this->comuna = $comuna;

        return $this;
    }

    /**
     * Get comuna
     *
     * @return \AppBundle\Entity\Comuna
     */
    public function getComuna()
    {
        return $this->comuna;
    }

    /**
     * Set area
     *
     * @param \AppBundle\Entity\Area $area
     *
     * @return Project
     */
    public function setArea(\AppBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \AppBundle\Entity\Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set class
     *
     * @param \AppBundle\Entity\ProjectClass $class
     *
     * @return Project
     */
    public function setClass(\AppBundle\Entity\ProjectClass $class = null)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return \AppBundle\Entity\ProjectClass
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set term
     *
     * @param \AppBundle\Entity\Term $term
     *
     * @return Project
     */
    public function setTerm(\AppBundle\Entity\Term $term = null)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return \AppBundle\Entity\Term
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set methodology
     *
     * @param \AppBundle\Entity\ProjectType $methodology
     *
     * @return Project
     */
    public function setMethodology(\AppBundle\Entity\ProjectType $methodology = null)
    {
        $this->methodology = $methodology;

        return $this;
    }

    /**
     * Get methodology
     *
     * @return \AppBundle\Entity\ProjectType
     */
    public function getMethodology()
    {
        return $this->methodology;
    }

    /**
     * Set specialist
     *
     * @param \AppBundle\Entity\User $specialist
     *
     * @return Project
     */
    public function setSpecialist(\AppBundle\Entity\User $specialist = null)
    {
        $this->specialist = $specialist;

        return $this;
    }

    /**
     * Get specialist
     *
     * @return \AppBundle\Entity\User
     */
    public function getSpecialist()
    {
        return $this->specialist;
    }

    /**
     * Set designer
     *
     * @param \AppBundle\Entity\User $designer
     *
     * @return Project
     */
    public function setDesigner(\AppBundle\Entity\User $designer = null)
    {
        $this->designer = $designer;

        return $this;
    }

    /**
     * Get designer
     *
     * @return \AppBundle\Entity\User
     */
    public function getDesigner()
    {
        return $this->designer;
    }

    /**
     * Set executive
     *
     * @param \AppBundle\Entity\User $executive
     *
     * @return Project
     */
    public function setExecutive(\AppBundle\Entity\User $executive = null)
    {
        $this->executive = $executive;

        return $this;
    }

    /**
     * Get executive
     *
     * @return \AppBundle\Entity\User
     */
    public function getExecutive()
    {
        return $this->executive;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stages = new ArrayCollection();
        $this->kind = new ArrayCollection();

        $this->modification = 'none';
        $this->currentStage = 1;
    }

    /**
     * Add stage
     *
     * @param \AppBundle\Entity\ProjectStage $stage
     *
     * @return Project
     */
    public function addStage(\AppBundle\Entity\ProjectStage $stage)
    {
        $this->stages[] = $stage;
        $stage->setProject($this);

        return $this;
    }

    /**
     * Remove stage
     *
     * @param \AppBundle\Entity\ProjectStage $stage
     */
    public function removeStage(\AppBundle\Entity\ProjectStage $stage)
    {
        $this->stages->removeElement($stage);

        $stage->setProject(null);
    }

    /**
     * Get stages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStages()
    {
        return $this->stages;
    }

    /**
     * Has stage
     *
     * @param \AppBundle\Entity\Stage $stage
     *
     * @return boolean
     */
    public function hasStage($stage)
    {
        foreach ($this->stages as $s)
        {
            if ($s->getStage()->getId() == $stage->getId())
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Project
     */
    public function setStatus($status)
    {
        if (!in_array($status, StatusEnum::getAvailableStatus())) {
            throw new \InvalidArgumentException("Invalid Status");
        }


        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return StatusEnum::getStatusName($this->status);
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getRealStatus()
    {
        return $this->status;
    }

    /**
     * ser currentStage
     *
     * @param integer $currentStage
     *
     * @return Project
     */
    public function setCurrentStage($currentStage)
    {
        $this->currentStage = $currentStage;

        return $this;
    }

    /**
     * Get currentStage
     *
     * @return integer
     */
    public function getCurrentStage()
    {
        return $this->currentStage;
    }

    /**
     * Get currentStage
     *
     * @return ProjectStage
     */
    public function getCurrentStageObj()
    {
        return $this->stages[$this->currentStage - 1];
    }

    public function getStageOrder($stage)
    {
        $order = 0;
        foreach ($this->stages as $s)
        {
            $order++;
            if($s->getStage()->getId() == $stage->getId())
            {
                break;
            }
        }

        return $order;
    }

    public function getStageByOrder($order)
    {
        return $this->stages[$order - 1];
    }

    public function getStageById($id)
    {
        foreach ($this->stages as $s)
        {
            if($s->getStage()->getId() == $id)
            {
                return $s;
            }
        }

        return null;
    }

    /**
     * Add log
     *
     * @param \AppBundle\Entity\ProjectLog $log
     *
     * @return Project
     */
    public function addLog(\AppBundle\Entity\ProjectLog $log)
    {
        $this->logs[] = $log;

        $log->setProject($this);

        return $this;
    }

    /**
     * Remove log
     *
     * @param \AppBundle\Entity\ProjectLog $log
     */
    public function removeLog(\AppBundle\Entity\ProjectLog $log)
    {
        $this->logs->removeElement($log);
    }

    /**
     * Get logs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogs()
    {
        return $this->logs;
    }

    public function getCurrentLog()
    {
        foreach ($this->logs as $log)
        {
            if($log->getIsActive())
            {
                return $log;
            }
        }

        return null;
    }

    public function getQuotedLog()
    {
        foreach ($this->logs as $log)
        {
            if($log->getStatus() == StatusEnum::STATUS_QUOTED)
            {
                return $log;
            }
        }

        return null;
    }

    /**
     * Add kind
     *
     * @param \AppBundle\Entity\ProjectKind $kind
     *
     * @return Project
     */
    public function addKind(\AppBundle\Entity\ProjectKind $kind)
    {
        $this->kind[] = $kind;

        return $this;
    }

    /**
     * Remove kind
     *
     * @param \AppBundle\Entity\ProjectKind $kind
     */
    public function removeKind(\AppBundle\Entity\ProjectKind $kind)
    {
        $this->kind->removeElement($kind);
    }

    /**
     * Get kind
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Set useMethodology
     *
     * @param boolean $useMethodology
     *
     * @return Project
     */
    public function setUseMethodology($useMethodology)
    {
        $this->useMethodology = $useMethodology;

        return $this;
    }

    /**
     * Get useMethodology
     *
     * @return boolean
     */
    public function getUseMethodology()
    {
        return $this->useMethodology;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        // check if the name is actually a fake name
        if ($this->getUseMethodology()) {
            if($this->getMethodology() == null)
            {
                $context->buildViolation('Este campo es obligatorio')
                    ->atPath('methodology')
                    ->addViolation();
            }
        }
    }

    public function getTimeStatus()
    {
        $today = new \DateTime('today');

        if($this->dateDifference($this->deliveryDate, $today) == '+' and $this->dateDifference($this->deliveryDate, $today, '%a') != 0)
        {
            return 'closed';
        }

        if($this->dateDifference($this->deliveryDate, $today, '%a') <= 2)
        {
            return 'to-close';
        }

        return 'in-time';
    }

    function dateDifference($date_1 , $date_2 , $differenceFormat = '%R' )
    {
        $interval = date_diff($date_1, $date_2);

        return $interval->format($differenceFormat);
    }

    /**
     * Set paymentStatus
     *
     * @param \AppBundle\Entity\PaymentStatus $paymentStatus
     *
     * @return Project
     */
    public function setPaymentStatus(\AppBundle\Entity\PaymentStatus $paymentStatus = null)
    {
        $this->paymentStatus = $paymentStatus;

        $paymentStatus->setProject($this);

        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return \AppBundle\Entity\PaymentStatus
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }
}
