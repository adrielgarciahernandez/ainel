<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="activity_complexity_value")
 */
class ActivityComplexityValue
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Activity", inversedBy="values")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     */
    private $activity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ActivityComplexity")
     * @ORM\JoinColumn(name="complexity_id", referencedColumnName="id")
     */
    private $complexity;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @Assert\Type(
     *     type="float",
     *     message="Valor no válido."
     * )
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "El valor debe ser mayor que 0.",
     * )
     *
     * @ORM\Column(type="float")
     */
    private $value;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return ActivityComplexityValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set activity
     *
     * @param \AppBundle\Entity\Activity $activity
     *
     * @return ActivityComplexityValue
     */
    public function setActivity(\AppBundle\Entity\Activity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \AppBundle\Entity\Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set complexity
     *
     * @param \AppBundle\Entity\ActivityComplexity $complexity
     *
     * @return ActivityComplexityValue
     */
    public function setComplexity(\AppBundle\Entity\ActivityComplexity $complexity = null)
    {
        $this->complexity = $complexity;

        return $this;
    }

    /**
     * Get complexity
     *
     * @return \AppBundle\Entity\ActivityComplexity
     */
    public function getComplexity()
    {
        return $this->complexity;
    }
}
