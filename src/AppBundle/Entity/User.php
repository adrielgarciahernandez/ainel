<?php
/**
 * User: adriel
 * Date: 4/4/2018
 * Time: 1:13 PM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"email"},
 *     errorPath="email",
 *     message="Ya existe un Trabajador con ese correo."
 * )
 * @UniqueEntity(
 *     fields={"rut"},
 *     errorPath="rut",
 *     message="Ya existe un Trabajador con ese rut."
 * )
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="string")
     */
    protected $rut;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @Assert\Length(
     *     min=3,
     *     max=255,
     *     minMessage="El nombre es muy corto.",
     *     maxMessage="El nombre es muy largo.",
     * )
     */
    protected $name;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="string")
     */
    protected $lastNames;


    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     *  @ORM\ManyToOne(targetEntity="AppBundle\Entity\Charge")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="charge", referencedColumnName="id")
     * })
     */
    protected $charge;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone;

    public function __construct()
    {
        parent::__construct();
    }

    public function __toString()
    {
        return $this->name . ' ' . $this->lastNames;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $lastNames
     */
    public function setLastNames($lastNames)
    {
        $this->lastNames = $lastNames;
    }

    /**
     * @return mixed
     */
    public function getLastNames()
    {
        return $this->lastNames;
    }

    /**
     * @return mixed
     */
    public function getCharge()
    {
        return $this->charge;
    }

    /**
     * @param mixed $charge
     */
    public function setCharge($charge)
    {
        $this->charge = $charge;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * @param mixed $rut
     */
    public function setRut($rut)
    {
        $this->rut = $rut;
    }

    public function getFullName()
    {
        return $this->name . ' ' . $this->lastNames;
    }

    public function getFullNameCharge()
    {
        return $this->name . ' ' . $this->lastNames . ' ( ' . $this->charge->getName() . ' )';
    }

    public function getRoles()
    {
        $roles = [];

        if ($this->charge->getIsProjectManager())
        {
            $roles[] = 'ROLE_PROJECT_MANAGER';
        }

        if ($this->charge->getIsProjectDirector())
        {
            $roles[] = 'ROLE_PROJECT_DIRECTOR';
        }

        return $roles;
    }
}