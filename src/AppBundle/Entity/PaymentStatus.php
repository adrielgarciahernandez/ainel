<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="payment_status")
 */
class PaymentStatus
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Project", inversedBy="paymentStatus", cascade={"persist"})
     * @ORM\JoinColumn(name="payment_status_id", referencedColumnName="id")
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PaymentStatusActivity", mappedBy="paymentStatus", cascade={"persist"})
     */
    private $activities;

    /**
     * @ORM\Column(type="integer")
     */
    private $code;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @Assert\Type(
     *     type="float",
     *     message="Valor no válido."
     * )
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "El valor debe ser mayor que 0.",
     * )
     *
     * @Assert\Range(
     *      max = 1,
     *      maxMessage = "El valor debe ser menor que 1.",
     * )
     * @ORM\Column(type="float")
     */
    private $bonus;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @Assert\Type(
     *     type="float",
     *     message="Valor no válido."
     * )
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "El valor debe ser mayor que 0.",
     * )
     *
     * @Assert\Range(
     *      max = 1,
     *      maxMessage = "El valor debe ser menor que 1.",
     * )
     * @ORM\Column(type="float")
     */
    private $penalty;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @Assert\Type(
     *     type="float",
     *     message="Valor no válido."
     * )
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "El valor debe ser mayor que 0.",
     * )
     *
     * @Assert\Range(
     *      max = 1,
     *      maxMessage = "El valor debe ser menor que 1.",
     * )
     * @ORM\Column(type="float")
     */
    private $factor;

    /**
     * @ORM\Column(type="float")
     */
    private $termFactor;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $termName;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->activities = new ArrayCollection();
        $this->factor = 0;
        $this->bonus = 0;
        $this->penalty = 0;
        $this->code = 0;
        $this->termFactor = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param integer $code
     *
     * @return PaymentStatus
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set bonus
     *
     * @param float $bonus
     *
     * @return PaymentStatus
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;

        return $this;
    }

    /**
     * Get bonus
     *
     * @return float
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * Set penalty
     *
     * @param float $penalty
     *
     * @return PaymentStatus
     */
    public function setPenalty($penalty)
    {
        $this->penalty = $penalty;

        return $this;
    }

    /**
     * Get penalty
     *
     * @return float
     */
    public function getPenalty()
    {
        return $this->penalty;
    }

    /**
     * Set factor
     *
     * @param float $factor
     *
     * @return PaymentStatus
     */
    public function setFactor($factor)
    {
        $this->factor = $factor;

        return $this;
    }

    /**
     * Get factor
     *
     * @return float
     */
    public function getFactor()
    {
        return $this->factor;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return PaymentStatus
     */
    public function setProject(\AppBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Add activity
     *
     * @param \AppBundle\Entity\PaymentStatusActivity $activity
     *
     * @return PaymentStatus
     */
    public function addActivity(\AppBundle\Entity\PaymentStatusActivity $activity)
    {
        $this->activities[] = $activity;

        $activity->setPaymentStatus($this);

        return $this;
    }

    public function hasActivity($activity)
    {
        foreach ($this->activities as $a)
        {
            if($activity->getId() == $a->getActivity()->getId())
            {
                return true;
            }
        }

        return false;
    }

    public function addActivityComplexity($activityId, $complexity)
    {
        foreach ($this->activities as $a)
        {
            if($activityId == $a->getActivity()->getId())
            {
                $a->setComplexity($complexity);
                $a->setPrice($a->getActivity()->getActivity()->getComplexityValue($complexity));

                return true;
            }
        }

        return false;
    }

    /**
     * Remove activity
     *
     * @param \AppBundle\Entity\PaymentStatusActivity $activity
     */
    public function removeActivity(\AppBundle\Entity\PaymentStatusActivity $activity)
    {
        $this->activities->removeElement($activity);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Set termFactor
     *
     * @param float $termFactor
     *
     * @return PaymentStatus
     */
    public function setTermFactor($termFactor)
    {
        $this->termFactor = $termFactor;

        return $this;
    }

    /**
     * Get termFactor
     *
     * @return float
     */
    public function getTermFactor()
    {
        return $this->termFactor;
    }

    /**
     * Set termName
     *
     * @param string $termName
     *
     * @return PaymentStatus
     */
    public function setTermName($termName)
    {
        $this->termName = $termName;

        return $this;
    }

    /**
     * Get termName
     *
     * @return string
     */
    public function getTermName()
    {
        return $this->termName;
    }
}
