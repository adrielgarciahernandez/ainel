<?php
/**
 * Created by PhpStorm.
 * User: adriel
 * Date: 4/25/2018
 * Time: 12:04 p.m.
 */

namespace AppBundle\Entity;


abstract class StatusEnum
{
    const STATUS_SAVED = "saved";
    const STATUS_ACTIVATED = "activated";
    const STATUS_FROZEN = "frozen";
    const STATUS_CANCELED = "canceled";
    const STATUS_APPROVED = "approved";
    const STATUS_QUOTED = "quoted";
    const STATUS_PAID = "paid";
    const STATUS_REVIEWED = "reviewed";

    /** @var array user friendly named status */
    protected static $statusName = [
        self::STATUS_SAVED => 'GUARDADO',
        self::STATUS_ACTIVATED => 'EN EJECUCIÓN',
        self::STATUS_FROZEN => 'PARALIZADO',
        self::STATUS_CANCELED => 'DEVUELTO',
        self::STATUS_APPROVED => 'APROBADO',
        self::STATUS_QUOTED => 'DESPACHADO',
        self::STATUS_PAID => 'PAGADO',
        self::STATUS_REVIEWED => 'EN REVISIÓN'
    ];

    /**
     * @param  string $statusShortName
     * @return string
     */
    public static function getStatusName($statusShortName)
    {
        if (!isset(static::$statusName[$statusShortName])) {
            return "Unknown type ($statusShortName)";
        }

        return static::$statusName[$statusShortName];
    }

    /**
     * @return array<string>
     */
    public static function getAvailableStatus()
    {
        return [
            self::STATUS_SAVED,
            self::STATUS_ACTIVATED,
            self::STATUS_FROZEN,
            self::STATUS_CANCELED,
            self::STATUS_APPROVED,
            self::STATUS_QUOTED,
            self::STATUS_PAID,
            self::STATUS_REVIEWED
        ];
    }

    public static function getStatusChoices()
    {
        return [
            'GUARDADO' => self::STATUS_SAVED,
            'EN EJECUCIÓN' => self::STATUS_ACTIVATED,
            'PARALIZADO' => self::STATUS_FROZEN,
            'DEVUELTO' => self::STATUS_CANCELED,
            'APROBADO' => self::STATUS_APPROVED,
            'DESPACHADO' => self::STATUS_QUOTED,
            'PAGADO' => self::STATUS_PAID,
            'EN REVISIÓN' => self::STATUS_REVIEWED
        ];
    }
}