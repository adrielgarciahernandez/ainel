<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="projectType_stage")
 */
class ProjectTypeStage
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $stageOrder;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProjectType", inversedBy="stages")
     * @ORM\JoinColumn(name="projectType_id", referencedColumnName="id")
     */
    private $projectType;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Stage")
     * @ORM\JoinColumn(name="stage_id", referencedColumnName="id")
     */
    private $stage;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Activity")
     * @ORM\JoinTable(name="projectType_stage_activity",
     *      joinColumns={@ORM\JoinColumn(name="projectTypeStage_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="activity_id", referencedColumnName="id")}
     *      )
     */
    private $activities;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stageOrder
     *
     * @param integer $stageOrder
     *
     * @return ProjectTypeStage
     */
    public function setStageOrder($stageOrder)
    {
        $this->stageOrder = $stageOrder;

        return $this;
    }

    /**
     * Get stageOrder
     *
     * @return integer
     */
    public function getStageOrder()
    {
        return $this->stageOrder;
    }

    /**
     * Set projectType
     *
     * @param \AppBundle\Entity\ProjectType $projectType
     *
     * @return ProjectTypeStage
     */
    public function setProjectType($projectType = null)
    {
        $this->projectType = $projectType;

        return $this;
    }

    /**
     * Get projectType
     *
     * @return \AppBundle\Entity\ProjectType
     */
    public function getProjectType()
    {
        return $this->projectType;
    }

    /**
     * Set stage
     *
     * @param \AppBundle\Entity\Stage $stage
     *
     * @return ProjectTypeStage
     */
    public function setStage($stage = null)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return \AppBundle\Entity\Stage
     */
    public function getStage()
    {
        return $this->stage;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->activities = new ArrayCollection();
    }

    /**
     * Add activity
     *
     * @param \AppBundle\Entity\Activity $activity
     *
     * @return ProjectTypeStage
     */
    public function addActivity($activity)
    {
        $this->activities[] = $activity;

        return $this;
    }

    /**
     * Has activity
     *
     * @param \AppBundle\Entity\Activity $activity
     *
     * @return boolean
     */
    public function hasActivity($activity)
    {
        foreach ($this->activities as $a)
        {
            if ($a->getId() == $activity->getId())
            {
                return true;
            }
        }

        return false;
    }
    
    /**
     * Remove activity
     *
     * @param \AppBundle\Entity\Activity $activity
     */
    public function removeActivity($activity)
    {
        $this->activities->removeElement($activity);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities()
    {
        return $this->activities;
    }
}
