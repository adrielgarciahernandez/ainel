<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="project_stage")
 */
class ProjectStage
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project", inversedBy="stages")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Stage")
     * @ORM\JoinColumn(name="stage_id", referencedColumnName="id")
     */
    private $stage;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProjectStageActivity", mappedBy="projectStage", cascade={"persist"})
     */
    private $activities;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="date", nullable=true)
     */
    private $programationStartDate;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="date", nullable=true)
     */
    private $programationEndDate;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="date", nullable=true)
     */
    private $executionStartDate;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="date", nullable=true)
     */
    private $executionEndDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->activities = new ArrayCollection();
    }

    /**
     * Set programationStartDate
     *
     * @param \DateTime $programationStartDate
     *
     * @return ProjectStage
     */
    public function setProgramationStartDate($programationStartDate)
    {
        $this->programationStartDate = $programationStartDate;

        return $this;
    }

    /**
     * Get programationStartDate
     *
     * @return \DateTime
     */
    public function getProgramationStartDate()
    {
        return $this->programationStartDate;
    }

    /**
     * Set programationEndDate
     *
     * @param \DateTime $programationEndDate
     *
     * @return ProjectStage
     */
    public function setProgramationEndDate($programationEndDate)
    {
        $this->programationEndDate = $programationEndDate;

        return $this;
    }

    /**
     * Get programationEndDate
     *
     * @return \DateTime
     */
    public function getProgramationEndDate()
    {
        return $this->programationEndDate;
    }

    /**
     * Set executionStartDate
     *
     * @param \DateTime $executionStartDate
     *
     * @return ProjectStage
     */
    public function setExecutionStartDate($executionStartDate)
    {
        $this->executionStartDate = $executionStartDate;

        return $this;
    }

    /**
     * Get executionStartDate
     *
     * @return \DateTime
     */
    public function getExecutionStartDate()
    {
        return $this->executionStartDate;
    }

    /**
     * Set executionEndDate
     *
     * @param \DateTime $executionEndDate
     *
     * @return ProjectStage
     */
    public function setExecutionEndDate($executionEndDate)
    {
        $this->executionEndDate = $executionEndDate;

        return $this;
    }

    /**
     * Get executionEndDate
     *
     * @return \DateTime
     */
    public function getExecutionEndDate()
    {
        return $this->executionEndDate;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return ProjectStage
     */
    public function setProject(\AppBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Add activity
     *
     * @param \AppBundle\Entity\ProjectStageActivity $activity
     *
     * @return ProjectStage
     */
    public function addActivity(\AppBundle\Entity\ProjectStageActivity $activity)
    {
        $this->activities[] = $activity;
        $activity->setProjectStage($this);

        return $this;
    }

    /**
     * Remove activity
     *
     * @param \AppBundle\Entity\ProjectStageActivity $activity
     */
    public function removeActivity(\AppBundle\Entity\ProjectStageActivity $activity)
    {
        $this->activities->removeElement($activity);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Set stage
     *
     * @param \AppBundle\Entity\Stage $stage
     *
     * @return ProjectStage
     */
    public function setStage(\AppBundle\Entity\Stage $stage = null)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return \AppBundle\Entity\Stage
     */
    public function getStage()
    {
        return $this->stage;
    }



    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProjectStage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    public function __toString()
    {
        return ''.$this->getId();
    }
}
