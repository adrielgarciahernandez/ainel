<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaymentFilterRepository")
 * @ORM\Table(name="payment_fitler")
 */
class PaymentFilter
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\ProjectKind")
     * @ORM\JoinTable(name="filter_payment_kind",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="kind_id", referencedColumnName="id")}
     *      )
     */
    private $kind;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Term")
     * @ORM\JoinTable(name="filter_payment_term",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="term_id", referencedColumnName="id")}
     *      )
     */
    private $term;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinTable(name="filter_payment_designer",
     *      joinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="designer_id", referencedColumnName="id")}
     *      )
     */
    private $designer;

    /**
     * @ORM\Column(type="date")
     */
    private $applicationDateBegin;

    /**
     * @ORM\Column(type="date")
     */
    private $applicationDateEnd;

    /**
     * @ORM\Column(type="date")
     */
    private $deliveryDateBegin;

    /**
     * @ORM\Column(type="date")
     */
    private $deliveryDateEnd;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->kind = new ArrayCollection();
        $this->term = new ArrayCollection();
        $this->designer = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set applicationDateBegin
     *
     * @param \DateTime $applicationDateBegin
     *
     * @return PaymentFilter
     */
    public function setApplicationDateBegin($applicationDateBegin)
    {
        $this->applicationDateBegin = $applicationDateBegin;

        return $this;
    }

    /**
     * Get applicationDateBegin
     *
     * @return \DateTime
     */
    public function getApplicationDateBegin()
    {
        return $this->applicationDateBegin;
    }

    /**
     * Set applicationDateEnd
     *
     * @param \DateTime $applicationDateEnd
     *
     * @return PaymentFilter
     */
    public function setApplicationDateEnd($applicationDateEnd)
    {
        $this->applicationDateEnd = $applicationDateEnd;

        return $this;
    }

    /**
     * Get applicationDateEnd
     *
     * @return \DateTime
     */
    public function getApplicationDateEnd()
    {
        return $this->applicationDateEnd;
    }

    /**
     * Set deliveryDateBegin
     *
     * @param \DateTime $deliveryDateBegin
     *
     * @return PaymentFilter
     */
    public function setDeliveryDateBegin($deliveryDateBegin)
    {
        $this->deliveryDateBegin = $deliveryDateBegin;

        return $this;
    }

    /**
     * Get deliveryDateBegin
     *
     * @return \DateTime
     */
    public function getDeliveryDateBegin()
    {
        return $this->deliveryDateBegin;
    }

    /**
     * Set deliveryDateEnd
     *
     * @param \DateTime $deliveryDateEnd
     *
     * @return PaymentFilter
     */
    public function setDeliveryDateEnd($deliveryDateEnd)
    {
        $this->deliveryDateEnd = $deliveryDateEnd;

        return $this;
    }

    /**
     * Get deliveryDateEnd
     *
     * @return \DateTime
     */
    public function getDeliveryDateEnd()
    {
        return $this->deliveryDateEnd;
    }

    /**
     * Add kind
     *
     * @param \AppBundle\Entity\ProjectKind $kind
     *
     * @return PaymentFilter
     */
    public function addKind(\AppBundle\Entity\ProjectKind $kind)
    {
        $this->kind[] = $kind;

        return $this;
    }

    /**
     * Remove kind
     *
     * @param \AppBundle\Entity\ProjectKind $kind
     */
    public function removeKind(\AppBundle\Entity\ProjectKind $kind)
    {
        $this->kind->removeElement($kind);
    }

    /**
     * Get kind
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Add term
     *
     * @param \AppBundle\Entity\Term $term
     *
     * @return PaymentFilter
     */
    public function addTerm(\AppBundle\Entity\Term $term)
    {
        $this->term[] = $term;

        return $this;
    }

    /**
     * Remove term
     *
     * @param \AppBundle\Entity\Term $term
     */
    public function removeTerm(\AppBundle\Entity\Term $term)
    {
        $this->term->removeElement($term);
    }

    /**
     * Get term
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Add designer
     *
     * @param \AppBundle\Entity\User $designer
     *
     * @return PaymentFilter
     */
    public function addDesigner(\AppBundle\Entity\User $designer)
    {
        $this->designer[] = $designer;

        return $this;
    }

    /**
     * Remove designer
     *
     * @param \AppBundle\Entity\User $designer
     */
    public function removeDesigner(\AppBundle\Entity\User $designer)
    {
        $this->designer->removeElement($designer);
    }

    /**
     * Get designer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesigner()
    {
        return $this->designer;
    }
}
