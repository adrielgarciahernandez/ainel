<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @UniqueEntity(
 *     fields={"name"},
 *     errorPath="name",
 *     message="Ya existe un Tipo de Proyecto con ese nombre."
 * )
 * @ORM\Entity
 * @ORM\Table(name="projectType")
 */
class ProjectType
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProjectTypeStage", mappedBy="projectType", cascade={"persist"})
     */
    private $stages;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProjectType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param boolean $description
     *
     * @return ProjectType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return boolean
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stages = new ArrayCollection();
    }

    /**
     * Add stage
     *
     * @param \AppBundle\Entity\ProjectTypeStage $stage
     *
     * @return ProjectType
     */
    public function addStage($stage)
    {
        $stage->setProjectType($this);
        $this->stages[] = $stage;

        return $this;
    }

    /**
     * Has stage
     *
     * @param \AppBundle\Entity\Stage $stage
     *
     * @return boolean
     */
    public function hasStage($stage)
    {
        foreach ($this->stages as $s)
        {
            if ($s->getStage()->getId() == $stage->getId())
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Remove stage
     *
     * @param \AppBundle\Entity\ProjectTypeStage $stage
     */
    public function removeStage($stage)
    {
        $this->stages->removeElement($stage);

        $stage->setProjectType(null);
    }

    /**
     * Get stages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStages()
    {
        return $this->stages;
    }

    public function getStageActivities($stage)
    {
        $stageObj = $this->stages[$stage - 1];

        return $stageObj->getActivities();
    }
}
