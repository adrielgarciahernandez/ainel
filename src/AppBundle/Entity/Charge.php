<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @UniqueEntity(
 *     fields={"name"},
 *     errorPath="name",
 *     message="Ya existe un Cargo con ese nombre."
 * )
 * @ORM\Entity
 * @ORM\Table(name="charge")
 */
class Charge
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isProjectManager;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isProjectDirector;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSpecialist;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isExecutive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isWorker;

    public function __toString()
    {
        return $this->name;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Charge
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isProjectManager
     *
     * @param boolean $isProjectManager
     *
     * @return Charge
     */
    public function setIsProjectManager($isProjectManager)
    {
        $this->isProjectManager = $isProjectManager;

        return $this;
    }

    /**
     * Get isProjectManager
     *
     * @return boolean
     */
    public function getIsProjectManager()
    {
        return $this->isProjectManager;
    }

    /**
     * Set isProjectDirector
     *
     * @param boolean $isProjectDirector
     *
     * @return Charge
     */
    public function setIsProjectDirector($isProjectDirector)
    {
        $this->isProjectDirector = $isProjectDirector;

        return $this;
    }

    /**
     * Get isProjectDirector
     *
     * @return boolean
     */
    public function getIsProjectDirector()
    {
        return $this->isProjectDirector;
    }

    /**
     * Set isSpecialist
     *
     * @param boolean $isSpecialist
     *
     * @return Charge
     */
    public function setIsSpecialist($isSpecialist)
    {
        $this->isSpecialist = $isSpecialist;

        return $this;
    }

    /**
     * Get isSpecialist
     *
     * @return boolean
     */
    public function getIsSpecialist()
    {
        return $this->isSpecialist;
    }

    /**
     * Set isExecutive
     *
     * @param boolean $isExecutive
     *
     * @return Charge
     */
    public function setIsExecutive($isExecutive)
    {
        $this->isExecutive = $isExecutive;

        return $this;
    }

    /**
     * Get isExecutive
     *
     * @return boolean
     */
    public function getIsExecutive()
    {
        return $this->isExecutive;
    }

    /**
     * Set isWorker
     *
     * @param boolean $isWorker
     *
     * @return Charge
     */
    public function setIsWorker($isWorker)
    {
        $this->isWorker = $isWorker;

        return $this;
    }

    /**
     * Get isWorker
     *
     * @return boolean
     */
    public function getIsWorker()
    {
        return $this->isWorker;
    }
}
