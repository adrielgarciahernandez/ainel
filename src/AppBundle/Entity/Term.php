<?php
/**
 * User: adriel
 * Date: 4/6/2018
 * Time: 7:44 AM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @UniqueEntity(
 *     fields={"name"},
 *     errorPath="name",
 *     message="Ya existe una Actividad con ese nombre."
 * )
 * @ORM\Entity
 * @ORM\Table(name="term")
 */
class Term
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Assert\NotBlank(message="Este campo es obligatorio")
     * @Assert\Type(
     *     type="float",
     *     message="Valor no válido."
     * )
     * @Assert\Range(
     *      min = 0,
     *      minMessage = "El valor debe ser mayor que 0.",
     * )
     *
     * @Assert\Range(
     *      max = 1,
     *      maxMessage = "El valor debe ser menor que 1.",
     * )
     * @ORM\Column(type="float")
     */
    private $increase;

    public function __toString()
    {
        return $this->name;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Term
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param boolean $description
     *
     * @return Term
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return boolean
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set increase
     *
     * @param float $increase
     *
     * @return Term
     */
    public function setIncrease($increase)
    {
        $this->increase = $increase;

        return $this;
    }

    /**
     * Get increase
     *
     * @return float
     */
    public function getIncrease()
    {
        return $this->increase;
    }
}
