<?php
/**
 * Created by PhpStorm.
 * User: adriel
 * Date: 5/9/2018
 * Time: 12:46 AM
 */

namespace AppBundle\Repository;

use AppBundle\Entity\PaymentFilter;
use AppBundle\Entity\StatusEnum;
use Doctrine\ORM\EntityRepository;

class PaymentFilterRepository extends EntityRepository
{
    public function filter(PaymentFilter $filter)
    {
        $filterDql = 'SELECT p FROM AppBundle:Project p
                      LEFT JOIN p.designer pdesigner
                      LEFT JOIN p.kind pkind
                      LEFT JOIN p.term pterm
                      WHERE p.status = :status ';

        $parameters = [];
        $parameters['status'] = StatusEnum::STATUS_QUOTED;

        $designerList = $filter->getDesigner();
        if ($designerList != null && count($designerList) > 0)
        {
            $filterDql .= 'AND (pdesigner.id = :designer'.$designerList[0]->getId();
            $parameters['designer'.$designerList[0]->getId()] = $designerList[0]->getId();

            for ($i = 1; $i < count($designerList); $i++)
            {
                $filterDql .= ' OR pdesigner.id = :designer'.$designerList[$i]->getId();
                $parameters['designer'.$designerList[$i]->getId()] = $designerList[$i]->getId();
            }

            $filterDql .= ') ';
        }

        $kindList = $filter->getKind();
        if ($kindList != null && count($kindList) > 0)
        {
            $filterDql .= 'AND (pkind.id = :kind'.$kindList[0]->getId();
            $parameters['kind'.$kindList[0]->getId()] = $kindList[0]->getId();

            for ($i = 1; $i < count($kindList); $i++)
            {
                $filterDql .= ' OR pkind.id = :kind'.$kindList[$i]->getId();
                $parameters['kind'.$kindList[$i]->getId()] = $kindList[$i]->getId();
            }

            $filterDql .= ') ';
        }

        $termList = $filter->getTerm();
        if ($termList != null && count($termList) > 0)
        {
            $filterDql .= 'AND (pterm.id = :term'.$termList[0]->getId();
            $parameters['term'.$termList[0]->getId()] = $termList[0]->getId();

            for ($i = 1; $i < count($termList); $i++)
            {
                $filterDql .= ' OR pterm.id = :term'.$termList[$i]->getId();
                $parameters['term'.$termList[$i]->getId()] = $termList[$i]->getId();
            }

            $filterDql .= ') ';
        }

        if ($filter->getApplicationDateBegin() != null)
        {
            $filterDql .= 'AND p.applicationDate >= :applicationDateBegin ';
            $parameters['applicationDateBegin'] = $filter->getApplicationDateBegin();
        }

        if ($filter->getApplicationDateEnd() != null)
        {
            $filterDql .= 'AND p.applicationDate <= :applicationDateEnd ';
            $parameters['applicationDateEnd'] = $filter->getApplicationDateEnd();
        }

        if ($filter->getDeliveryDateBegin() != null)
        {
            $filterDql .= 'AND p.deliveryDate >= :deliveryDateBegin ';
            $parameters['deliveryDateBegin'] = $filter->getDeliveryDateBegin();
        }

        if ($filter->getDeliveryDateEnd() != null)
        {
            $filterDql .= 'AND p.deliveryDate <= :deliveryDateEnd ';
            $parameters['deliveryDateEnd'] = $filter->getDeliveryDateEnd();
        }

        return $this->getEntityManager()
            ->createQuery($filterDql)
            ->setParameters($parameters)
            ->getResult();
    }
}