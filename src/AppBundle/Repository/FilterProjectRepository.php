<?php
/**
 * Created by PhpStorm.
 * User: adriel
 * Date: 5/9/2018
 * Time: 12:46 AM
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\FilterProject;

class FilterProjectRepository extends EntityRepository
{
    public function filter(FilterProject $filter)
    {
        $filterDql = 'SELECT p FROM AppBundle:Project p
                      LEFT JOIN p.class pclass
                      LEFT JOIN p.area parea
                      LEFT JOIN p.designer pdesigner
                      LEFT JOIN p.executive pexecutive
                      LEFT JOIN p.specialist pspecialist
                      LEFT JOIN p.comuna pcomuna
                      LEFT JOIN p.kind pkind
                      LEFT JOIN p.term pterm
                      WHERE 1=1 ';

        $parameters = [];

        if ($filter->getName() != null)
        {
            $filterDql .= 'AND p.name LIKE :nombre ';
            $parameters['nombre'] = '%'.$filter->getName().'%';
        }

        $classList = $filter->getClass();
        if ($classList != null && count($classList) > 0)
        {
            $filterDql .= 'AND (pclass.id = :class'.$classList[0]->getId();
            $parameters['class'.$classList[0]->getId()] = $classList[0]->getId();

            for ($i = 1; $i < count($classList); $i++)
            {
                $filterDql .= ' OR pclass.id = :class'.$classList[$i]->getId();
                $parameters['class'.$classList[$i]->getId()] = $classList[$i]->getId();
            }

            $filterDql .= ') ';
        }

        $areaList = $filter->getArea();
        if ($areaList != null && count($areaList) > 0)
        {
            $filterDql .= 'AND (parea.id = :area'.$areaList[0]->getId();
            $parameters['area'.$areaList[0]->getId()] = $areaList[0]->getId();

            for ($i = 1; $i < count($areaList); $i++)
            {
                $filterDql .= ' OR parea.id = :area'.$areaList[$i]->getId();
                $parameters['area'.$areaList[$i]->getId()] = $areaList[$i]->getId();
            }

            $filterDql .= ') ';
        }

        $designerList = $filter->getDesigner();
        if ($designerList != null && count($designerList) > 0)
        {
            $filterDql .= 'AND (pdesigner.id = :designer'.$designerList[0]->getId();
            $parameters['designer'.$designerList[0]->getId()] = $designerList[0]->getId();

            for ($i = 1; $i < count($designerList); $i++)
            {
                $filterDql .= ' OR pdesigner.id = :designer'.$designerList[$i]->getId();
                $parameters['designer'.$designerList[$i]->getId()] = $designerList[$i]->getId();
            }

            $filterDql .= ') ';
        }

        $executiveList = $filter->getExecutive();
        if ($executiveList != null && count($executiveList) > 0)
        {
            $filterDql .= 'AND (pexecutive.id = :executive'.$executiveList[0]->getId();
            $parameters['executive'.$executiveList[0]->getId()] = $executiveList[0]->getId();

            for ($i = 1; $i < count($executiveList); $i++)
            {
                $filterDql .= ' OR pexecutive.id = :executive'.$executiveList[$i]->getId();
                $parameters['executive'.$executiveList[$i]->getId()] = $executiveList[$i]->getId();
            }

            $filterDql .= ') ';
        }

        $specialistList = $filter->getSpecialist();
        if ($specialistList != null && count($specialistList) > 0)
        {
            $filterDql .= 'AND (pspecialist.id = :specialist'.$specialistList[0]->getId();
            $parameters['specialist'.$specialistList[0]->getId()] = $specialistList[0]->getId();

            for ($i = 1; $i < count($specialistList); $i++)
            {
                $filterDql .= ' OR pspecialist.id = :specialist'.$specialistList[$i]->getId();
                $parameters['specialist'.$specialistList[$i]->getId()] = $specialistList[$i]->getId();
            }

            $filterDql .= ') ';
        }
        
        if ($filter->getAddress() != null)
        {
            $filterDql .= 'AND p.address LIKE :address ';
            $parameters['address'] = '%'.$filter->getAddress().'%';
        }

        $comunaList = $filter->getComuna();
        if ($comunaList != null && count($comunaList) > 0)
        {
            $filterDql .= 'AND (pcomuna.id = :comuna'.$comunaList[0]->getId();
            $parameters['comuna'.$comunaList[0]->getId()] = $comunaList[0]->getId();

            for ($i = 1; $i < count($comunaList); $i++)
            {
                $filterDql .= ' OR pcomuna.id = :comuna'.$comunaList[$i]->getId();
                $parameters['comuna'.$comunaList[$i]->getId()] = $comunaList[$i]->getId();
            }

            $filterDql .= ') ';
        }

        $kindList = $filter->getKind();
        if ($kindList != null && count($kindList) > 0)
        {
            $filterDql .= 'AND (pkind.id = :kind'.$kindList[0]->getId();
            $parameters['kind'.$kindList[0]->getId()] = $kindList[0]->getId();

            for ($i = 1; $i < count($kindList); $i++)
            {
                $filterDql .= ' OR pkind.id = :kind'.$kindList[$i]->getId();
                $parameters['kind'.$kindList[$i]->getId()] = $kindList[$i]->getId();
            }

            $filterDql .= ') ';
        }

        $statusList = $filter->getStatus();
        if ($statusList != null && count($statusList) > 0)
        {
            $filterDql .= 'AND (p.status = :status0';
            $parameters['status0'] = $statusList[0];

            for ($i = 1; $i < count($statusList); $i++)
            {
                $filterDql .= ' OR p.status = :status'.$i;
                $parameters['status'.$i] = $statusList[$i];
            }

            $filterDql .= ') ';
        }

        $termList = $filter->getTerm();
        if ($termList != null && count($termList) > 0)
        {
            $filterDql .= 'AND (pterm.id = :term'.$termList[0]->getId();
            $parameters['term'.$termList[0]->getId()] = $termList[0]->getId();

            for ($i = 1; $i < count($termList); $i++)
            {
                $filterDql .= ' OR pterm.id = :term'.$termList[$i]->getId();
                $parameters['term'.$termList[$i]->getId()] = $termList[$i]->getId();
            }

            $filterDql .= ') ';
        }

        if ($filter->getApplicationDateBegin() != null)
        {
            $filterDql .= 'AND p.applicationDate >= :applicationDateBegin ';
            $parameters['applicationDateBegin'] = $filter->getApplicationDateBegin();
        }

        if ($filter->getApplicationDateEnd() != null)
        {
            $filterDql .= 'AND p.applicationDate <= :applicationDateEnd ';
            $parameters['applicationDateEnd'] = $filter->getApplicationDateEnd();
        }

        if ($filter->getDeliveryDateBegin() != null)
        {
            $filterDql .= 'AND p.deliveryDate >= :deliveryDateBegin ';
            $parameters['deliveryDateBegin'] = $filter->getDeliveryDateBegin();
        }

        if ($filter->getDeliveryDateEnd() != null)
        {
            $filterDql .= 'AND p.deliveryDate <= :deliveryDateEnd ';
            $parameters['deliveryDateEnd'] = $filter->getDeliveryDateEnd();
        }

        return $this->getEntityManager()
            ->createQuery($filterDql)
            ->setParameters($parameters)
            ->getResult();
    }
}