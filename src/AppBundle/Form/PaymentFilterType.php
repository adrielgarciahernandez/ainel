<?php

namespace AppBundle\Form;

use AppBundle\Entity\StatusEnum;
use Doctrine\DBAL\Types\JsonArrayType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaymentFilterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kind', null, ['label' => 'Tipos', 'required' => false])
            ->add('term', null, ['label' => 'Plazos', 'required' => false])
            ->add('applicationDateBegin', DateType::class, array(
                'required'=>false,
                'label'=>'Fecha de Solicitud Mayor que',
                'widget' => 'single_text',
                'format' => 'dd/M/yyyy',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // add a class that can be selected in JavaScript
                'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
            ))
            ->add('applicationDateEnd', DateType::class, array(
                'required'=>false,
                'label'=>'Fecha de Solicitud Menor que',
                'widget' => 'single_text',
                'format' => 'dd/M/yyyy',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // add a class that can be selected in JavaScript
                'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
            ))
            ->add('deliveryDateBegin', DateType::class, array(
                'required'=>false,
                'label'=>'Fecha de Entrega Mayor que',
                'widget' => 'single_text',
                'format' => 'dd/M/yyyy',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // add a class that can be selected in JavaScript
                'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
            ))
            ->add('deliveryDateEnd', DateType::class, array(
                'required'=>false,
                'label'=>'Fecha de Entrega Menor que',
                'widget' => 'single_text',
                'format' => 'dd/M/yyyy',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // add a class that can be selected in JavaScript
                'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
            ))
            ->add('designer', EntityType::class, array(
                'class' => 'AppBundle:User',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.charge', 'ch')
                        ->where('ch.isProjectDirector = ?1')
                        ->setParameter(1, true);
                },
                'choice_label' => 'fullName',
                'label' => 'Proyectistas',
                'required' => false,
                'multiple'  => true
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\PaymentFilter'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_payment_filter';
    }


}
