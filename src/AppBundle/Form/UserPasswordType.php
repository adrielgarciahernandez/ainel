<?php
/**
 * User: adriel
 * Date: 4/8/2018
 * Time: 5:03 PM
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('plainPassword', RepeatedType::class, array(
                    'required' => false,
                    'type' => PasswordType::class,
                    'options' => array(
                        'attr' => array(
                            'autocomplete' => 'new-password',
                        ),
                    ),
                    'first_options' => array('required' => false,
                                             'label' => 'Contraseña',
                                             'constraints' => array(
                                                new NotBlank(array('message'=>'Este campo es obligatorio')),
                                              )),
                    'second_options' => array('required' => false,
                                              'label' => 'Confirmar Contraseña',
                                              'constraints' => array(
                                                 new NotBlank(array('message'=>'Este campo es obligatorio')),
                                              )),
                    'invalid_message' => 'Las contraseñas no coinciden',
                ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }
}