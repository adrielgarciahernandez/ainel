<?php
/**
 * User: adriel
 * Date: 4/8/2018
 * Time: 5:03 PM
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', null, ['required' => false, 'label'=>'Nombre'])
                ->add('rut', null, ['required' => false, 'label'=>'RUT'])
                ->add('lastNames', null, ['required' => false, 'label'=>'Apellidos'])
                ->add('charge', null, ['required' => false, 'label'=>'Cargo'])
                ->add('phone', null, ['required' => false, 'label'=>'Teléfono'])
                ->add('email', EmailType::class,
                            array('required' => false,
                                  'label' => 'Correo',
                                'constraints' => array(
                                    new NotBlank(array('message'=>'Este campo es obligatorio')),
                                    new Email(array('message'=>'Debe escribir un email válido')),
                                )));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }
}