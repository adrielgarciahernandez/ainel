<?php

namespace AppBundle\Form;

use AppBundle\Entity\StatusEnum;
use Doctrine\DBAL\Types\JsonArrayType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FilterProjectType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['label' => 'Nombre Contiene', 'required' => false])
            ->add('address', null, ['label' => 'Dirección Contiene', 'required' => false])
            ->add('comuna', null, ['label' => 'Comunas', 'required' => false])
            ->add('area', null, ['label' => 'Areas', 'required' => false])
            ->add('kind', null, ['label' => 'Tipos', 'required' => false])
            ->add('class', null, ['label' => 'Clases', 'required' => false])
            ->add('term', null, ['label' => 'Plazos', 'required' => false])
            ->add('status',
                ChoiceType::class,
                [
                    'label' => 'Estado',
                    'choices'=>StatusEnum::getStatusChoices(),
                    'required' => false,
                    'multiple'  => true
                ])
            ->add('applicationDateBegin', DateType::class, array(
                'required'=>false,
                'label'=>'Fecha de Solicitud Mayor que',
                'widget' => 'single_text',
                'format' => 'dd/M/yyyy',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // add a class that can be selected in JavaScript
                'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
            ))
            ->add('applicationDateEnd', DateType::class, array(
                'required'=>false,
                'label'=>'Fecha de Solicitud Menor que',
                'widget' => 'single_text',
                'format' => 'dd/M/yyyy',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // add a class that can be selected in JavaScript
                'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
            ))
            ->add('deliveryDateBegin', DateType::class, array(
                'required'=>false,
                'label'=>'Fecha de Entrega Mayor que',
                'widget' => 'single_text',
                'format' => 'dd/M/yyyy',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // add a class that can be selected in JavaScript
                'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
            ))
            ->add('deliveryDateEnd', DateType::class, array(
                'required'=>false,
                'label'=>'Fecha de Entrega Menor que',
                'widget' => 'single_text',
                'format' => 'dd/M/yyyy',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // add a class that can be selected in JavaScript
                'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
            ))
            ->add('specialist', EntityType::class, array(
                'class' => 'AppBundle:User',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.charge', 'ch')
                        ->where('ch.isSpecialist = ?1')
                        ->setParameter(1, true);
                },
                'choice_label' => 'fullName',
                'label' => 'Especialistas',
                'required' => false,
                'multiple'  => true
            ))
            ->add('executive', EntityType::class, array(
                'class' => 'AppBundle:User',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.charge', 'ch')
                        ->where('ch.isExecutive = ?1')
                        ->setParameter(1, true);
                },
                'choice_label' => 'fullName',
                'label' => 'Ejecutivos',
                'required' => false,
                'multiple'  => true
            ))
            ->add('designer', EntityType::class, array(
                'class' => 'AppBundle:User',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->join('u.charge', 'ch')
                        ->where('ch.isProjectDirector = ?1')
                        ->setParameter(1, true);
                },
                'choice_label' => 'fullName',
                'label' => 'Proyectistas',
                'required' => false,
                'multiple'  => true
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\FilterProject'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_filter_project';
    }


}
