<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('ot', null, ['label' => 'OT', 'required' => false])
                ->add('otNumber', null, ['label' => 'Numero de OT', 'required' => false])
                ->add('modification', null, ['label' => 'Modificación', 'required' => false])
                ->add('address', null, ['label' => 'Dirección', 'required' => false])
                ->add('comuna', null, ['label' => 'Comuna', 'required' => false])
                ->add('area', null, ['label' => 'Area', 'required' => false])
                ->add('kind', null, ['label' => 'Tipos', 'required' => false])
                ->add('class', null, ['label' => 'Clase', 'required' => false])
                ->add('term', null, ['label' => 'Plazo', 'required' => false])
                ->add('methodology', null, ['label' => 'Metodología de Trabajo', 'required' => false])
                ->add('useMethodology', null, ['label' => 'Utilizar Metodología de Trabajo', 'required' => false])
                ->add('description', null, ['label' => 'Observaciones', 'required' => false])
                ->add('specialist', EntityType::class, array(
                    'class' => 'AppBundle:User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->join('u.charge', 'ch')
                            ->where('ch.isSpecialist = ?1')
                            ->setParameter(1, true);
                    },
                    'choice_label' => 'fullName',
                    'label' => 'Especialista',
                    'required' => false
                ))
                ->add('executive', EntityType::class, array(
                    'class' => 'AppBundle:User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->join('u.charge', 'ch')
                            ->where('ch.isExecutive = ?1')
                            ->setParameter(1, true);
                    },
                    'choice_label' => 'fullName',
                    'label' => 'Ejecutivo',
                    'required' => false
                ))
                ->add('designer', EntityType::class, array(
                    'class' => 'AppBundle:User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->join('u.charge', 'ch')
                            ->where('ch.isProjectDirector = ?1')
                            ->setParameter(1, true);
                    },
                    'choice_label' => 'fullName',
                    'label' => 'Proyectista',
                    'required' => false
                ))
                ->add('applicationDate', DateType::class, array(
                    'required'=>false,
                    'label'=>'Fecha de Solicitud',
                    'widget' => 'single_text',
                    'format' => 'dd/M/yyyy',
                    // do not render as type="date", to avoid HTML5 date pickers
                    'html5' => false,

                    // add a class that can be selected in JavaScript
                    'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
                ))
            ->add('deliveryDate', DateType::class, array(
                'required'=>false,
                'label'=>'Fecha de Entrega',
                'widget' => 'single_text',
                'format' => 'dd/M/yyyy',
                // do not render as type="date", to avoid HTML5 date pickers
                'html5' => false,

                // add a class that can be selected in JavaScript
                'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Project'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_project';
    }


}
