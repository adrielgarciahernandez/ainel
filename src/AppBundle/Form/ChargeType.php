<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChargeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', null, ['label' => 'Nombre', 'required' => false])
                ->add('isProjectManager', null, ['label' => '¿Es Coordinador de Proyectos?'])
                ->add('isProjectDirector', null, ['label' => '¿Es Director de Proyectos?'])
                ->add('isExecutive', null, ['label' => '¿Es Ejecutivo?'])
                ->add('isSpecialist', null, ['label' => '¿Es Especialista?'])
                ->add('isWorker', null, ['label' => '¿Es Trabajador Regular?']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Charge'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_charge';
    }


}
