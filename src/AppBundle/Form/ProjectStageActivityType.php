<?php

namespace AppBundle\Form;

use AppBundle\Entity\Project;
use AppBundle\Entity\Stage;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectStageActivityType extends AbstractType
{
    private $project;
    private $stage;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->project = $options['project'];
        $this->stage = $options['stage'];

        if ($this->project->getUseMethodology())
        {
            $activities = $this->project->getMethodology()->getStageActivities($this->project->getStageOrder($this->stage));
        }
        else
        {
            $activities = $this->stage->getActivities();
        }


        $builder->add('description', null, ['label' => 'Observaciones', 'required' => false])
                ->add('executor', EntityType::class, array(
                    'class' => 'AppBundle:User',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->join('u.charge', 'ch')
                            ->where('ch.isSpecialist = ?1 and ch.isExecutive = ?1')
                            ->setParameter(1, false);
                    },
                    'choice_label' => 'fullNameCharge',
                    'label' => 'Ejecutor',
                    'required' => false
                ))
                ->add('activity', EntityType::class, array(
                    'class' => 'AppBundle:Activity',
                    'choices' => $activities,
                    'choice_label' => 'name',
                    'label' => 'Baremo',
                    'required' => false
                ))
                ->add('startDate', DateType::class, array(
                    'required'=>false,
                    'label'=>'Inicio',
                    'widget' => 'single_text',
                    'format' => 'dd/M/yyyy',
                    // do not render as type="date", to avoid HTML5 date pickers
                    'html5' => false,

                    // add a class that can be selected in JavaScript
                    'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
                ))
                ->add('endDate', DateType::class, array(
                    'required'=>false,
                    'label'=>'Fin',
                    'widget' => 'single_text',
                    'format' => 'dd/M/yyyy',
                    // do not render as type="date", to avoid HTML5 date pickers
                    'html5' => false,

                    // add a class that can be selected in JavaScript
                    'attr' => array('class' => 'form-control', 'data-date-format'=>'dd/mm/yyyy', 'text-type'=>'date-picker'),
                ))
                ->add('isProgrammed', null, ['label' => '¿Es un Baremo Adicional?', 'required' => false])
                ->add('count', null, ['label' => 'Cantidad', 'required' => false])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ProjectStageActivity',
            'project' => null
        ));

        $resolver->setRequired('project');
        $resolver->setRequired('stage');
        $resolver->setAllowedTypes('project', Project::class);
        $resolver->setAllowedTypes('stage', Stage::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_projectStageActivity';
    }


}
